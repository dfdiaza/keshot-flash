package com.dedosmedia.component
{
	import feathers.controls.Button;
import feathers.skins.ImageSkin;

import flash.geom.Rectangle;

import starling.events.Event;
import starling.textures.Texture;

import starling.display.Image;

public class Button extends feathers.controls.Button
	{
		
		private var _sound:String;

        private var _callback:String;

        private var _args:Array = new Array()
		
		public function Button()
		{
			super();
            this.addEventListener(Event.TRIGGERED, button_triggeredHandler)
		}
		
		public function set sound(_sound:String):void
		{
			this._sound = _sound
		}
		
		public function get sound():String
		{
			return this._sound;
		}

        public function set args(_args:Array):void
        {
            this._args = _args
        }

        public function get args():Array
        {
            return this._args;
        }


        public function set callback(_callback:String):void
        {
            this._callback = _callback
        }

        public function get callback():String
        {
            return this._callback;
        }

        public function set source(_texture:Texture):void
        {

            var skin:ImageSkin = new ImageSkin( _texture );
            skin.width = this.width;
            skin.height = this.height;
            this.defaultSkin = skin;

        }


        public function button_triggeredHandler(e:Event):void
		{
            trace("BUTTON TRIGGER INTERNAL");
		}

        override public function dispose():void
        {

            this.removeEventListener(Event.TRIGGERED, button_triggeredHandler)
            super.dispose();

        }
	}
}