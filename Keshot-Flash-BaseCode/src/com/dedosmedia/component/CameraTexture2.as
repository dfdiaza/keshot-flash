package com.dedosmedia.component
{
	import flash.display.BitmapData;
	import flash.geom.Point;
import flash.geom.Rectangle;
import flash.geom.Rectangle;
	import flash.media.Camera;

	import starling.display.Image;
	import starling.textures.Texture;
	import starling.utils.RectangleUtil;
	import starling.display.Sprite;
import starling.utils.ScaleMode;

public class CameraTexture2 extends Sprite implements ICameraTexture
	{

		private var _camera:flash.media.Camera;
		private var _cameraIndex:String;
		private var _fps:Number = 30;
		private var _quality:int = 100;
		private var _bandwidth:int = 0;
    	private var _mode:String

		public var _image:starling.display.Image = new starling.display.Image(null);


		private var _frame:starling.display.Image = new starling.display.Image(null);
		private var _frameIndex:uint = 0;
		private var _frameTexture:Vector.<Texture>;

		private var _streamRect:Rectangle = new Rectangle(0,0,640,480);   			// Resolución de la webcam
        private var _viewportRect:Rectangle = new Rectangle(0,0,0,0);		// El viewport rect que queremos ver
        private var _viewportPosition:Point = new Point(0,0)					// posicion del viewport en la imagen de salida


		private var _cameraTexture:Texture;

		public function CameraTexture2()
		{
			super();
			this.addChild(this._image);

			trace("CTX::: "+this.x," CTY::::",this.y);
		}

		public function start(_cameraIndex:String, _frameTexture:Vector.<Texture>=null):void
		{
            this._frameTexture = _frameTexture;
            if(this._frameTexture)
            {
                this.addChild(this._frame);
            }

            this._cameraIndex = _cameraIndex;

            this._camera = flash.media.Camera.getCamera(this._cameraIndex);
			this._camera.setMode(this._streamRect.width,this._streamRect.height,this._fps);
			this._camera.setQuality(this._bandwidth,this._quality);
            trace(" CAMERA TEXTURE2:::::: Camera Size: ",this._camera.width, this._camera.height);


			// TODO: cuando no hay camara lanza error


            this._cameraTexture = Texture.fromCamera(this._camera,1, function():void
			{


				var cameraRect:Rectangle = RectangleUtil.fit(viewportRect, streamRect, ScaleMode.SHOW_ALL);

				// Per default it's working with SHOW_ALL, usar mode para seleccionar modo
				//var cameraRect2:Rectangle = RectangleUtil.fit(viewportRect, streamRect, ScaleMode.NONE);


				var cameraScale:Number = cameraRect.width/viewportRect.width;
				trace("Camera Scale",cameraScale);
                var subTexture:Texture = Texture.fromTexture(_cameraTexture, cameraRect,viewportRect,false,cameraScale);
				_image.texture = subTexture;
				_image.readjustSize();
				update();
			});

		}
		public function loadFrame(_texture:Texture):void {

		}

		public function showFrame(_visible:Boolean):void
		{
			trace("show F: ",this._frame);
			if(this._frame)
			{
                this._frame.visible = _visible;
			}
		}

		public function next():void
		{
            if(this._frameTexture) {
                this._frameIndex = ++this._frameIndex > this._frameTexture.length - 1 ? 0 : this._frameIndex;
                update();
            }
		}

		public function back():void
		{
            if(this._frameTexture) {
                this._frameIndex = --this._frameIndex < 0 ? this._frameTexture.length - 1 : this._frameIndex;
                update();
            }
		}

		private function update():void
		{
			if(this._frameTexture)
			{
                if(this._frameTexture.length>0)
                {
                    this._frame.texture = this._frameTexture[this._frameIndex];
                    this._frame.readjustSize();
                }
			}
		}

		public function set streamRect(_streamRect:Rectangle):void
		{
			this._streamRect = _streamRect;
		}

		public function get streamRect():Rectangle
		{
			return this._streamRect;
		}

        public function set viewportRect(_viewportRect:Rectangle):void
        {
            this._viewportRect = _viewportRect;
        }

        public function get viewportRect():Rectangle
        {
            return this._viewportRect;
        }

        public function set viewportPosition(_viewportPosition:Point):void
        {
            this._viewportPosition = _viewportPosition;
        }

        public function get viewportPosition():Point
        {
            return this._viewportPosition;
        }

		public function set mode(_mode:String):void
		{
			this._mode = _mode;
		}

		public function get mode():String
		{
			return this._mode;
		}


		public function set fps(_fps:Number):void
		{
			this._fps = _fps;
		}

		public function get fps():Number
		{
			return this._fps;
		}

		public function set bandwidth(_bandwidth:int):void
		{
			this._bandwidth = _bandwidth;
		}

		public function get bandwidth():int
		{
			return this._bandwidth;
		}

		public function set quality(_quality:int):void
		{
			this._quality = _quality;
		}

		public function get quality():int
		{
			return this._quality;
		}

		public function getBitmapData():BitmapData
		{
			return this.drawToBitmapData();
		}

		override public function dispose():void
		{


			trace("dispoSeCamera2 (*)")
			if(_image.texture)
			{
				_image.texture.root.attachCamera(null);
				_image.texture.dispose();
				_image.texture = null;

				while(this.numChildren>0)
				{
					this.removeChildAt(0,true);
				}
			}

            if(this._cameraTexture)
            {
				trace("root? "+this._cameraTexture.root)
                this._cameraTexture.dispose();
                this._cameraTexture = null;
            }


            super.dispose();
		}
	}
}