/**
 * Created by dedosmedia on 23/02/17.
 */
package com.dedosmedia.component {


import flash.geom.Matrix;
import flash.geom.Point;

import starling.animation.Transitions;

import starling.core.Starling;
import starling.events.Event;

import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.textures.Texture;
import starling.utils.Pool;

public class DraggableImage extends Image {


    public static const DROPPED:String = "draggableDropped";


    private var _delta: Point;

    private var _m:Matrix;
    private var touches:Vector.<Touch>;
    private var _json:Object;

    // almacenar la matriz de transformacion original del objeto antes de ser movido
    override public function set transformationMatrix(_m:Matrix):void
    {
        super.transformationMatrix = _m;

        if(this._m == null)
        {
            this._m = _m.clone();
            this.alignPivot();
            this.x += this.pivotX;
            this.y += this.pivotY;
        }
    }

    public function DraggableImage(_json:Object) {
        super();

        this._json = _json;

        this.addEventListener(TouchEvent.TOUCH, onTouch);
    }

    public function onTouch(event:TouchEvent):void {
        touches = event.getTouches(this, TouchPhase.MOVED);
        if(touches)
        {
            if(touches.length == 1)
            {
                this._delta = touches[0].getMovement(parent,Pool.getPoint());
                this.x += this._delta.x;
                this.y += this._delta.y;
                Pool.putPoint(this._delta);
                this.touchable = false;

                this.parent.addChild(this);

            }
        }

        touches = event.getTouches(this, TouchPhase.ENDED);
        if(touches)
        {
            if(touches.length == 1)
            {

                this.dispatchEventWith(DraggableImage.DROPPED,true,{json:this._json, touch:touches[0]});


                //Reset position
                this.transformationMatrix = this._m;
                this.alignPivot();
                this.x += this.pivotX;
                this.y += this.pivotY;

                this.scale = 0.1;
                Starling.juggler.tween(this,1,{
                    transition: Transitions.EASE_OUT_ELASTIC,
                    onComplete: function():void { touchable= true;},
                    scaleX: 1,
                    scaleY: 1
                })
            }
        }



    }

    override public function dispose():void
    {
        this.removeEventListener(TouchEvent.TOUCH, onTouch);
        super.dispose();
    }


}
}
