/**
 * Created by dedosmedia on 18/02/17.
 */
package com.dedosmedia.component {

import flash.geom.Point;
import flash.geom.Rectangle;

import starling.animation.Transitions;

import starling.core.Starling;
import starling.display.DisplayObjectContainer;

import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.filters.FragmentFilter;
import starling.textures.Texture;
import starling.textures.TextureSmoothing;
import starling.utils.Pool;

public class TouchSheet extends Sprite{


    public static const DROPPED:String = "touchSheetDropped";
    public static const CHOSEN:String = "touchSheetChosen";

    private var _box:starling.display.Image;
    private var _image:starling.display.Image;
    private var _isTranslating:Boolean = false;

    private var _delta: Point;
    private var _currentVector:Point;
    private var _previousVector:Point;
    private var _pivot:Point = new Point();
    private var _zero:Point ;

    private var _content:Sprite;

    private var _handleTR:Sprite;
    private var _cornerTR:Point;
    private var _TR:Point;

    private var _handleBL:Sprite;
    private var _cornerBL:Point;
    private var _BL:Point;

    private var _translationChanged:Boolean = true;

    private var _hasFocus:Boolean = false;


    private var _bringToFront:Boolean = true;
    private var _isRemovable:Boolean = true;

    private var _borderScaleFactor:Point = new Point();

    public function TouchSheet(_texture:Texture, _iconTR:Texture, _iconBL:Texture, _border:Texture, _iconsEnabled:Boolean = true) {



        this._borderScaleFactor.x = _texture.nativeWidth/_border.nativeWidth;
        this._borderScaleFactor.y = _texture.nativeHeight/_border.nativeHeight;



        this.filter = new FragmentFilter();
        this.filter.antiAliasing = 16;



        this._box           = new starling.display.Image(_border)
        this._box.scale9Grid = new Rectangle(5,5,10,10);
        this._image         = new starling.display.Image(_texture);
        this._image.pixelSnapping = true;
        this._image.textureSmoothing = TextureSmoothing.TRILINEAR;



        this._content       = new Sprite();
        this._TR            = new Point(_texture.nativeWidth, 0);
        this._BL            = new Point(0,_texture.nativeHeight);
        this._handleTR      = new Sprite();
        this._handleBL      = new Sprite();

        this._content.addChild(this._image);
        this._content.alignPivot();
        this.addChild(this._content);

        if(_iconsEnabled)
        {

            this._handleTR.addChild(new starling.display.Image(_iconTR))
            this._handleTR.alignPivot();
            this._handleBL.addChild(new starling.display.Image(_iconBL))
            this._handleBL.alignPivot();
            this.addChild(this._handleTR);
            this.addChild(this._handleBL);
        }



        this.addEventListener(TouchEvent.TOUCH, onTouch);
        this.useHandCursor = true;

        this.alignPivot();
        this._zero = new Point(this.pivotX, this.pivotY)

        this.addChild(_box);
        this._box.alignPivot();
        this._box.alpha = 0.5;
        this._box.touchable = false;
        this.setChildIndex(this._box,0);

    }

    public function get bringToFront():Boolean
    {
        return this._bringToFront;
    }

    public function set bringToFront(_bringToFront:Boolean):void
    {
        this._bringToFront = _bringToFront;
    }

    public function get isRemovable():Boolean
    {
        return this._isRemovable;
    }

    public function set isRemovable(_isRemovable:Boolean):void
    {
        this._isRemovable = _isRemovable;
    }

    public function setSize(_w:Number, _h:Number):void
    {
        this._content.width = _w;
        this._content.height = _h;
        this.update();
    }

    public function set focus(_hasFocus:Boolean):void
    {
        this._hasFocus = _hasFocus;
        this._box.visible = this._handleBL.visible =  this._handleTR.visible = this._hasFocus;
        this.touchGroup = !this._hasFocus;
        if(this._hasFocus ){
            this.update();
        }
    }

    public function get focus():Boolean
    {
        return this._hasFocus;
    }

    private function isTranslating(event:TouchEvent):Boolean
    {
        this._isTranslating = !(event.getTouches(this._handleTR, TouchPhase.MOVED).length > 0) &&  !(event.getTouches(this._handleBL, TouchPhase.MOVED).length > 0)
        return this._isTranslating
    }

    private function onTouch(event:TouchEvent):void
    {

        var touches:Vector.<Touch> = event.getTouches(this, TouchPhase.BEGAN);
        if(touches.length == 1) {
            this.dispatchEventWith(TouchSheet.CHOSEN, true, {touch:touches[0], target: this});
        }

        touches = event.getTouches(this, TouchPhase.ENDED);
        if(touches.length == 1) {
            this.dispatchEventWith(TouchSheet.DROPPED, true, {touch:touches[0], target: this});
        }

        touches = event.getTouches(this, TouchPhase.MOVED);

        if(touches.length == 1)
        {
            if(this.isTranslating(event))
            {
                this._translationChanged = true;
                this._delta = touches[0].getMovement(parent,Pool.getPoint());
                this.x += this._delta.x;
                this.y += this._delta.y;
                Pool.putPoint(this._delta);
            }
            else
            {

                if(this._translationChanged)
                {
                    this._pivot = localToGlobal(this._zero);
                    this._translationChanged = false;
                }

                var touchA:Touch = event.touches[0];
                var currentPosA:Point = touchA.getLocation(parent, Pool.getPoint());
                var previousPosA:Point = touchA.getPreviousLocation(parent,  Pool.getPoint());

                var currentPosB:Point = this._pivot;
                var previousPosB:Point = this._pivot;

                _currentVector = currentPosA.subtract(currentPosB);
                _previousVector = previousPosA.subtract(previousPosB); // scale
                var scaleDiff:Number = _currentVector.length / _previousVector.length;
                this._content.scale *= scaleDiff;

                var currentAngle:Number = Math.atan2(_currentVector.y, _currentVector.x);
                var previousAngle:Number = Math.atan2(_previousVector.y, _previousVector.x);
                var angleDiff:Number = currentAngle - previousAngle;
                this._content.rotation += angleDiff;

                Pool.putPoint(currentPosA);
                Pool.putPoint(previousPosA);
            }
        }

        if(touches.length > 0)
        {
            update();
        }

    }

    private function update():void
    {
        this._cornerTR = this._content.localToGlobal(_TR, Pool.getPoint());
        this._cornerTR = globalToLocal(_cornerTR);
        this._cornerBL = this._content.localToGlobal(_BL, Pool.getPoint());
        this._cornerBL = globalToLocal(_cornerBL);

        this._handleTR.x = this._cornerTR.x;
        this._handleTR.y = this._cornerTR.y;
        this._handleTR.rotation = this._content.rotation;
        this._handleBL.x = this._cornerBL.x;
        this._handleBL.y = this._cornerBL.y;
        this._handleBL.rotation = this._content.rotation;

        Pool.putPoint(this._cornerTR);
        Pool.putPoint(this._cornerBL);

        this._box.rotation = this._content.rotation;
        this._box.scaleX = this._content.scaleX*this._borderScaleFactor.x;
        this._box.scaleY = this._content.scaleY*this._borderScaleFactor.y;

    }

    public function destroy():void
    {
        removeFromParent(true);
    }

    override  public function dispose():void
    {
        while(numChildren>0)
        {
            removeChildAt(0,true);
        }
        super.dispose();
    }

}
}
