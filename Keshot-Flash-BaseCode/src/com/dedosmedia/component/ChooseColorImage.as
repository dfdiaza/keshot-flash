package com.dedosmedia.component
{
    import feathers.controls.ImageLoader;
    import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class ChooseColorImage extends feathers.controls.ImageLoader
	{

		public static const CHOOSING_MOVED:String = "choosingMoved";
        public static const CHOOSING_BEGAN:String = "choosingBegan";
        public static const CHOOSING_ENDED:String = "choosingEnded";

		private var _touch:Touch;

		public function ChooseColorImage()
		{
			super();
		}

		public function get touch():Touch
		{
			return this._touch;
		}

		override protected function initialize():void
		{
			trace("Iniclizado");
			this.addEventListener(TouchEvent.TOUCH, onTouch);
		}

		protected function onTouch(event:TouchEvent):void
		{
			var touches:Vector.<Touch> = event.getTouches(this,TouchPhase.MOVED || TouchPhase.BEGAN || TouchPhase.ENDED);
			if(touches.length == 1)
			{
                _touch = touches[0];
                trace("PHASE ",_touch.phase)
                switch(_touch.phase)
                {
                    case TouchPhase.BEGAN:
                            this.dispatchEventWith(ChooseColorImage.CHOOSING_BEGAN,false,{touch:_touch});
                        break;
                    case TouchPhase.ENDED:
                            this.dispatchEventWith(ChooseColorImage.CHOOSING_ENDED,false,{touch:_touch});
                        break;
                    case TouchPhase.MOVED:
                            this.dispatchEventWith(ChooseColorImage.CHOOSING_MOVED,false,{touch:_touch});
                        break;

                }



			}



		}

        override public function dispose():void
        {
            super.dispose();
        }
	}
}