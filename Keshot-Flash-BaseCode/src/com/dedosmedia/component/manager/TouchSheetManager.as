/**
 * Created by dedosmedia on 21/02/17.
 */
package com.dedosmedia.component.manager {
import com.dedosmedia.component.*;

import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;
import starling.display.Stage;
import starling.events.Event;
import starling.events.EventDispatcher;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class TouchSheetManager extends EventDispatcher {

    public static const DROPPED:String = "touchSheetDropped";
    public static const DRAGGED:String = "touchSheetDragged";
    public static const DEPTH_CHANGED:String = "touchSheetDepthChanged"

    private var _sheets:Vector.<TouchSheet>;

    private var _currentFocus:TouchSheet;

    private var _stage:Stage;

    public function TouchSheetManager(_stage:Stage=null) {
        super();

        if(_stage)
        {
            this._stage = _stage;
            this._stage.addEventListener(TouchEvent.TOUCH, onTouch);
        }

        this._sheets = new Vector.<TouchSheet>();
    }


    // CHeck is the parent object is from an Class
    private function checkParentType(_object:DisplayObject, _type:Class):Boolean
    {
        var _parent:DisplayObjectContainer = _object.parent;
        if(_parent)
        {
            if(_parent is _type)
            {
                return true;
            }
            else
            {
                return checkParentType(_parent,_type);
            }

        }
        return false;
    }

    private function onTouch(event:TouchEvent):void {
        var touches:Vector.<Touch> = event.getTouches(this._stage,TouchPhase.BEGAN);
        if(touches.length == 1)
        {
            // Matar foco si se toca cualquier objeto que no pertenezca a un TouchSheet
            if(!(event.target is TouchSheet))
            {
                if (!checkParentType(event.target as DisplayObject,TouchSheet))
                {
                    this.killFocus();
                }
            }
        }
    }


    public function get sheets():Vector.<TouchSheet>
    {
        return this._sheets;
    }

    public function bringToFront(_sheet:TouchSheet):void
    {
        if(this.returnSheet(_sheet) != -1 && _sheet.bringToFront)
        {
            _sheet.parent.addChild(_sheet);
            this.dispatchEventWith(TouchSheetManager.DEPTH_CHANGED);
        }
    }

    public function add(_sheet:TouchSheet):void
    {
        trace(this.returnSheet(_sheet))
        if(this.returnSheet(_sheet) == -1)
        {
            this._sheets.push(_sheet);
            _sheet.addEventListener(TouchSheet.CHOSEN, onFocus);
            _sheet.addEventListener(TouchSheet.DROPPED, onDrop);
            this.setFocus(_sheet);
        }
    }

    public function killFocus():void
    {
        if(this._currentFocus)
        {
            this._currentFocus.focus = false;
        }
    }

    public function setFocus(_sheet:TouchSheet):void
    {
        if(this.returnSheet(_sheet) != -1)
        {
            if(this._currentFocus)
            {
                this._currentFocus.focus = false;
            }

            this._currentFocus = _sheet;
            this._currentFocus.focus = true;


            this.bringToFront(_sheet);
        }

    }

    private function onFocus(event:Event):void {
        this.setFocus(TouchSheet(event.target));
        this.dispatchEventWith(TouchSheetManager.DRAGGED,true,{target:event.target, touch:event.data.touch});
    }



    private function onDrop(event:Event):void {
        this.dispatchEventWith(TouchSheetManager.DROPPED,true,{target:event.target, touch:event.data.touch});
    }


    public function remove(_sheet:TouchSheet):Boolean
    {
        var _index:int = this.returnSheet(_sheet);
        if( _index != -1){
            var _removedSheet:TouchSheet = this._sheets.removeAt(_index) as TouchSheet;
            TouchSheet(_removedSheet).removeEventListener(TouchSheet.CHOSEN, onFocus);
            TouchSheet(_removedSheet).removeEventListener(TouchSheet.DROPPED, onDrop);
            return true
        }
        return false;
    }

    private function returnSheet(_sheet:TouchSheet):int
    {
        for(var i:uint = 0, l:uint = this._sheets.length; i < l; i++)
        {
            if(_sheet == this._sheets[i])
            {
                return i;
            }
        }
        return -1;
    }

    public function dispose():void
    {
        if(_stage)
        {
            this._stage.addEventListener(TouchEvent.TOUCH, onTouch);
            this._stage = null;
        }

        while(this._sheets.length > 0)
        {
            this.remove(this._sheets[this._sheets.length-1]);
        }

    }
}
}
