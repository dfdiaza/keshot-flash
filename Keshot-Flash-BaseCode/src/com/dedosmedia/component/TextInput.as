package com.dedosmedia.component
{
	import feathers.controls.TextInput;
	
	import starling.events.Event;
	public class TextInput extends feathers.controls.TextInput
	{
		
		private var _required:Boolean = false;
		private var _validation:String = "";
		private var _error:String = null;
		private var _formatString:String = "";
		private var _formatArray:Array;
		private var _inputArray:Array;
		private var _resultArray:Array = new Array();
		
		private var _autoUpdate:Boolean = false;
		
		public function TextInput()
		{
			super();
		}
		
		public function set required(_required:Boolean):void
		{
			this._required = _required
		}
		
		public function get required():Boolean
		{
			return this._required;
		}
		
		public function set error(_error:String):void
		{
			this._error = _error
		}
		
		public function get error():String
		{
			return this._error;
		}
		
		public function set formatString(_formatString:String):void
		{
			this._formatArray = _formatString.split("");
			this._formatString = _formatString
		}
		
		public function get formatString():String
		{
			return this._formatString;
		}
		
		
		public function set validation(_validation:String):void
		{
			this._validation = _validation
		}
		
		public function get validation():String
		{
			return this._validation;
		}
		
		public function button_triggeredHandler(e:Event):void
		{
			trace("BUTTON TRIGGER");
		}
		
		public function format():void
		{
			
			if(this._autoUpdate == true)
			{
				this._autoUpdate = false;
				return;
			}
			
			this._resultArray.length = 0;
			
			var regEx:RegExp = /[^0-9.]/g;
			var noNumbersName:String = this.text.replace(regEx, '');
			trace(this.text, noNumbersName);
			this._inputArray = noNumbersName.split("");
			var latestPosition:uint = 0;
			for(var i:uint = 0, j:uint = 0, l:uint = this._formatArray.length;i<l;i++)
			{
				if(this._formatArray[i] == "#" && j<this._inputArray.length)
				{
					//replace
					trace("replace ",j,this._inputArray[j])
					this._resultArray.push(this._inputArray[j++])
					latestPosition = i;
				}
				else
				{
					//poner
					trace("place ",i,this._formatArray[i])
					this._resultArray.push(this._formatArray[i])
				}
				
			}
			trace("result",this._resultArray.join(""),latestPosition)
			this._autoUpdate = true;
			
			this.text = this._resultArray.join("");
			this.selectRange(latestPosition+1,latestPosition+1);
			
			
			
			 
		}
		 
		public function isValid():Boolean
		{			
			var expression:RegExp = new RegExp(this.validation);
			 
			trace(expression.source,this.validation,this.text,expression.test(this.text))
			var _test:Boolean = !expression.test(this.text)
			
			trace("Test: ",_test);
			this.errorString = _test?error:null
			
			return !expression.test(this.text);
		}
		
		override public function dispose():void
		{
			
			super.dispose();
		}
	}
}