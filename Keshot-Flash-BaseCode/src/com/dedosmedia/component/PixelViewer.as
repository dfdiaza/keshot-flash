/**
 * Created by dedosmedia on 18/02/17.
 */
package com.dedosmedia.component {

import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;

import starling.display.BlendMode;


import starling.display.Image;
import starling.display.Sprite;
import starling.textures.Texture;
import starling.utils.Pool;

public class PixelViewer extends Sprite{

    private var _box:starling.display.Image;                // Imagen con el borde de la caja
    private var _image:starling.display.Image;              // imagen con la textura de la que queremos extraer el punto
    private var _zero:Point ;
    private var _cross:starling.display.Image;              // Imagen con la cruz
    private var _texture:Texture;                           // La textura del preview
    private var _point:Point;                               // El punto del preview eleegido

    private var _subtextureWidth:Number;
    private var _subtextureHeight:Number;

    private var _subTexture:Texture;

    private var _pixelSelected:Boolean = false;

    public function get pixelSelected():Boolean
    {
        return this._pixelSelected;
    }

    public function PixelViewer( _width:Number, _height:Number, _cross:Texture, _border:Texture) {

        this._image         = new starling.display.Image(null);
        this.addChild(this._image);

        this._box           = new starling.display.Image(_border)
        this._box.scale9Grid = new Rectangle(5,5,10,10);
        this._box.width = _width;
        this._box.height = _height;
        this.addChild(this._box);

        this._cross         = new starling.display.Image(_cross);
        this._cross.x += _width/2-_cross.nativeWidth/2;
        this._cross.y += _height/2-_cross.nativeHeight/2;
        this._cross.blendMode = BlendMode.SCREEN;

        this._zero = new Point(this._cross.x, this._cross.y);
        this.addChild(this._cross);

        this._subtextureWidth = _width;
        this._subtextureHeight = _height;


        this.touchable = false;


    }

    public function set texture(_texture:Texture):void
    {
        this._texture = _texture;
    }

    public function get texture():Texture
    {
        return this._texture
    }

    public function showPoint(_point:Point):void
    {
        if(this._subTexture)
        {
            this._subTexture.dispose();
            this._subTexture = null;
        }

        this._subTexture =  Texture.fromTexture(this._texture, new Rectangle(_point.x-this._subtextureWidth/2, _point.y-this._subtextureHeight/2, this._subtextureWidth, this._subtextureHeight));
        this._image.texture = this._subTexture;
        this._image.readjustSize();

        this._pixelSelected = true;
    }

    public function getPixelColor():BitmapData
    {
        var _width:Number = this._cross.width;
        var _height:Number = this._cross.height;
        var pixelArea:Rectangle = Pool.getRectangle(0,0,_width,_height);

        pixelArea.offset((this._subtextureWidth-_width)/2, (this._subtextureHeight-_height)/2)


        var pixelTexture:Texture =  Texture.fromTexture(this._subTexture, pixelArea);
        var tempImg:starling.display.Image = new starling.display.Image(pixelTexture);
        var bmd:BitmapData = tempImg.drawToBitmapData();
        Pool.putRectangle(pixelArea);
        return  bmd;
    }

    override  public function dispose():void
    {
        while(numChildren>0)
        {
            removeChildAt(0,true);
        }
        super.dispose();
    }

}
}
