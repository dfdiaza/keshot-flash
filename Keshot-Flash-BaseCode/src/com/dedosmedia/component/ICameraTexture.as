/**
 * Created by dedosmedia on 11/02/17.
 */
package com.dedosmedia.component {
import flash.display.BitmapData;

import starling.textures.Texture;

public interface ICameraTexture {

    function getBitmapData():BitmapData;
    function showFrame(show:Boolean):void;
    function loadFrame(_texture:Texture):void;
}
}
