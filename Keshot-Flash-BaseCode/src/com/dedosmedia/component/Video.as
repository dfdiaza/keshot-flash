package com.dedosmedia.component
{
	import feathers.controls.ImageLoader;
	import feathers.media.VideoPlayer;
	
	import starling.events.Event;
	import feathers.events.FeathersEventType;


	public class Video extends VideoPlayer
	{

		private var loader:ImageLoader = new ImageLoader();
		
		private var disposed:Boolean = false;
		
		public function Video()
		{
			super();
			this.addChild( loader );			
			this.addEventListener( Event.READY, videoPlayer_readyHandler );
			this.addEventListener( feathers.events.FeathersEventType.CLEAR, videoPlayer_clearHandler );
			this.addEventListener(Event.COMPLETE, videoPlayer_completeHandler);
			
			
			
			trace("VideoSource ")
		}
		
		public function start(source:String):void
		{
			trace("PLAY A: "+source)
			this.videoSource = source;
		}
		
		private function videoPlayer_clearHandler( event:Event ):void
		{
			loader.source = null;
			trace("CLEAR") 
		}
		
		
		public function videoPlayer_readyHandler(event:Event):void
		{
			trace("VIDEO READY")
			loader.source = this.texture
		}
		
		public function videoPlayer_completeHandler():void
		{
			trace("COMPLETE");
			this.seek(0);
		}
		
		override public function dispose():void
		{
			trace(" [DISPOSE VIDEO COMPONENT ] " )
			if(!this.disposed)
			{
				this.texture.root.attachNetStream(null);
				this.texture.dispose();
				this.disposed = true;
			}
			
			while(this.numChildren>0) 
			{
				this.removeChildAt(0,true);
			}
		
			this.removeEventListener( Event.READY, videoPlayer_readyHandler );
			this.removeEventListener( feathers.events.FeathersEventType.CLEAR, videoPlayer_clearHandler );
			this.removeEventListener(Event.COMPLETE, videoPlayer_completeHandler);
			
			super.dispose();
		}
	}
}