package com.dedosmedia.component
{
    import feathers.controls.ImageLoader;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.events.Event;
import flash.utils.ByteArray;

import starling.display.Image;

import starling.events.Event;

	public class Image extends feathers.controls.ImageLoader
	{

		private var _saveBitmapData:Boolean = false;
		private var _bmd:BitmapData;




		public function Image()
		{
			super();
		}

		public function set saveBitmapData(_saveBitmapData:Boolean):void
		{
			this._saveBitmapData = _saveBitmapData;
		}

        public function get saveBitmapData():Boolean
        {
            return this._saveBitmapData;
        }

		public function get bmd():BitmapData
		{
			return this._bmd;
		}

		public function get internalImage():starling.display.Image
		{
			return this.image;
		}

        override public function dispose():void
        {
			if(_bmd)
			{
				_bmd.dispose();
				_bmd = null;
			}

            super.dispose();
        }


		override protected function loader_completeHandler(event:flash.events.Event):void
		{
			trace("LOADER COMPLETED ",this.name )
			if(_saveBitmapData)
			{
				if(_bmd)
				{
					_bmd.dispose();
				}
                _bmd = Bitmap(this.loader.content).bitmapData.clone();
				trace("bmd ",_bmd.rect);
			}

			super.loader_completeHandler(event);
		}

	}
}