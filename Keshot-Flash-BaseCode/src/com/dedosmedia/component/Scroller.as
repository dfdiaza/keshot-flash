package com.dedosmedia.component
{
	import feathers.controls.ImageLoader;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollInteractionMode;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	
	public class Scroller extends ScrollContainer
	{
		
		private var _scroller_layout:HorizontalLayout = new HorizontalLayout();
		private var _gap:Number = 0;
		private var _padding:Number = 0;
		private var _item:Array = new Array();
		
		private var _image:Vector.<ImageLoader> = new Vector.<ImageLoader>();
		
		
		public function Scroller()
		{
			super();
	
			this._scroller_layout.verticalAlign = VerticalAlign.MIDDLE;
			this.layout = this._scroller_layout;
			this.interactionMode = ScrollInteractionMode.TOUCH;
		}
		
		public function set gap(_gap:Number):void
		{
			this._gap = _gap;
			this._scroller_layout.gap = _gap;
		}
		
		public function get gap():Number
		{
			return this._gap;
		}
		
		public function set item(_item:Array):void
		{
			this._item = _item;		
			while(this.numChildren>0)
				this.removeChildAt(0,true);
			
			for(var y:int = 0, y_max:int = this._item.length; y < y_max; y++)
			{
				this.addChild(this._item[y]);
			}
		}
		
		public function get item():Array
		{
			return this._item;
		}
		
		public override function dispose():void
		{
			super.dispose();
			//this.backgroundSkin.dispose();
			trace("**** SE NECESITA REMOVER backgrounSkin on Scroller?")
		}
		/*
		var skin:Image = new Image( texture );
		skin.scale9Grid = new Rectangle( 2, 2, 1, 6 );
		container.backgroundSkin = skin;
		*/

		
	}
}