package com.dedosmedia.services
{
	import com.dedosmedia.events.ErrorEventType;
	import com.dedosmedia.events.FileEventType;
	

    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;

	
	import org.robotlegs.starling.mvcs.Actor;
	
	public class FileReadJSONService extends Actor implements IFileReadService
	{
        private var _urlRequest:URLRequest = new URLRequest();
        private var _urlLoader:URLLoader = new URLLoader();
		private var _filePath:String = "";
        private var _jsonData:Object;


		public function read(_filePath:String):void
		{
			this._filePath = _filePath;
            this._urlRequest.url = _filePath;
            this._urlLoader.addEventListener(IOErrorEvent.IO_ERROR, urlLoader_onIOError)
            this._urlLoader.addEventListener(Event.COMPLETE, urlLoader_onComplete);
			try
			{
                this._urlLoader.load(this._urlRequest);
            }
			catch (e:Error)
			{
				var msg:String = "[ERROR] "+e.errorID+" :: "+e.getStackTrace();
				this.dispatchWith(ErrorEventType.ERROR,true,{file:"FileReadJSONServicevice.as", text:msg})
			}
		}

        private function urlLoader_onIOError(e:IOErrorEvent):void {

            var msg:String = "[ERROR] "+ e.errorID+" :: Fichero no existe o no se puede leer: "+this._filePath+" "+e.text;
            this.dispatchWith(ErrorEventType.ERROR,true,{file:"FileReadJSONServicevice.as", text:msg})
        }

        private function urlLoader_onComplete(event:Event):void {
                try{
                    this._jsonData = JSON.parse(this._urlLoader.data);
                }
                catch(e:Error)
                {
                    var msg:String = "[ERROR] "+ e.errorID+" :: No se puede decodificar el JSON: "+this._filePath+" "+e.text;
                    trace(msg)
                    this.dispatchWith(ErrorEventType.ERROR,true,{file:"FileReadJSONService.ase.as", text:msg})
                    return;
                }

                this.dispatchWith(FileEventType.FILE_READ_COMPLETED, true, {data:JSON.parse(this._urlLoader.data)});
        }

		
		
	}
}