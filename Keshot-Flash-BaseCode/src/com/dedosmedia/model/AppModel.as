package com.dedosmedia.model
{
	
	//import com.dedosmedia.interfaces.IFWVideoEncoder;

	import com.dedosmedia.model.vo.ConfigVO;
	import com.dedosmedia.model.vo.SnapshotVO;
	import com.dedosmedia.utils.TwoWayLocalConnection;

	import flash.display.BitmapData;
	import flash.geom.Rectangle;
import flash.system.Capabilities;

import org.robotlegs.starling.mvcs.Actor;
	
	public class AppModel extends Actor
	{


        public function AppModel()
        {
            this._config = new ConfigVO();
        }

		private var _config:ConfigVO;
        public function get config():ConfigVO
        {
            return this._config;
        }
        public function set config(_config:ConfigVO):void
        {
            this._config = _config;
        }





		// Ruta en disco donde se buscarán proyectos
		private var _path:String = ""
		
		// Ruta al directorio de proyecto actualmente en ejecución
		private var _projectDirectory:String;
		
		
		// Ruta al directorio de proyecto actualmente en ejecución
		private var _os:String;
		
		
		private var _size:Rectangle;
		
		private var _design_size:Rectangle;
		
		
		//private var _playerNumber:uint = 0;
		private var _gamePlan:Object;
		private var _currentPlayer:Object; 
		
		
		private var _localConnection:TwoWayLocalConnection;


		public function get isIDE():Boolean
		{
            if(Capabilities.isDebugger && Capabilities.playerType != "ActiveX" )
			{
				return true;
			}

			return false;
		}



		private var _url:String;

		public function get url():String
		{
			return this._url;
		}
		public function set url(_url:String):void
		{
			this._url = _url;
		}



		// Archivo de configuración inicial
		
		
		private var _timestamp:String;



		// Array donde se guardan los path a las imagenes de entrada sin procesar y al color
		private var _inputFilesPath:Array = new Array();


        public function get inputFilesPath():Array
        {
            return this._inputFilesPath;
        }


		// ----------------




        // Carpeta donde se guardan las fotos
        private var _snapshot_path:String;


        // Contiene los Bitmapdta de las fotos capturadas en la session actual
        private var _snapshot:SnapshotVO = new SnapshotVO();
		public function get snapshot():SnapshotVO
		{
			return this._snapshot;
		}
		
		public function set snapshot(_snapshot:SnapshotVO):void
		{
			if(this._snapshot)
			{
				this._snapshot.dispose();
				this._snapshot = null;
			}
			this._snapshot = _snapshot;
		}
        public function get snapshot_path():String
        {

            return this._snapshot_path;
        }

        public function set snapshot_path(_snapshot_path:String):void
        {

            this._snapshot_path = _snapshot_path;
        }


        public function get localConnection():TwoWayLocalConnection
        {
            return this._localConnection;
        }

        public function set localConnection(_localConnection:TwoWayLocalConnection):void
        {
            this._localConnection = _localConnection;
        }

		public function get timestamp():String
		{
			return this._timestamp;
		}
		
		public function set timestamp(_timestamp:String):void
		{
			this._timestamp = _timestamp;
		}
		

		
		/*
		public function get playerNumber():uint
		{
			return this._playerNumber;
		}
		
		public function set playerNumber(_playerNumber:uint):void
		{
			this._playerNumber = _playerNumber;
		}
		*/
		
		
		public function get currentPlayer():Object
		{
			return this._currentPlayer;
		}
		
		public function set currentPlayer(_currentPlayer:Object):void
		{
			this._currentPlayer = _currentPlayer;
		}
		
		public function get gamePlan():Object
		{
			return this._gamePlan;
		}
		
		public function set gamePlan(_gamePlan:Object):void
		{
			this._gamePlan = _gamePlan;
		}
		
		
		
		public function get path():String
		{
			return this._path;
		}
		
		public function get projectDirectory():String
		{
			return this._projectDirectory;
		}
		
		public function set projectDirectory(file:String):void
		{
			this._projectDirectory = file;
		}
		
		public function get os():String
		{
			return this._os;
		}
		
		public function set os(_os:String):void
		{
			this._os = _os;
		}
		
		
		public function get size():Rectangle
		{
			return this._size;
		}
		
		public function set size(_value:Rectangle):void
		{
			this._size = _value;
		}
		
		
		public function get design_size():Rectangle
		{
			return this._design_size;
		}
		
		public function set design_size(_value:Rectangle):void
		{
			this._design_size = _value;
		}

		
		
	}
}


