/**
 * Created by dedosmedia on 27/12/16.
 */
package com.dedosmedia.model.vo {
public class ConfigVO {


    private var _params:Object;                          // Todos los parametros recibidos por el Flash
    private var _language:String = "en";                // Lenguaje de la app, usado para cargar textos o imagenes lozalizadas
    private var _sessionType:String = "";               // Modo de funcionamiento seleccionado por el usuario.  por defecto "" (no cambiar), para cambiar modo default en debug, poner uno en config.json
    private var _selectedBackground:String = "";               // Un string con el background que debemos poner, en caso de ser dinámico y elegido antes de cargar el Flash capture.
    private var _selectedOverlay:String = "";               // Un string con el overlay que debemos poner, en caso de ser dinámico y elegido antes de cargar el Flash capture (o en Overlay).
    private var _selectedObject:Object;               // Un Objecto con algunos settings elegidos cuando se escoge un overlay en OverlaySelectionScreen
    private var _jsonPath:String = "";               // Nombre de la subcarpeta del json, que contiene los arcvhivos de configuración (depende del archivo config.json (global) )
    private var _photosPerSession:uint = 1;             // Número de tomas que se realizarán en esta sesión (variará automaticamente según el sessionType elegido, se modifica en UpdateConfigVOCommand)
    private var _photosTimePerSession:Number = 0;           // Tiempo durnte el cual se capturan X gifPhotos
    private var _previewTimeBeforeCaptureNextPicture:uint = 0;    // Indica el tiempo que se mostrará el preview de la foto tomada, antes de realizar la siguiente toma en segundos (cuando son más de 1 tomas)
    private var _showPreviewAfterCapture:Boolean = false;         // Indica si se muestra o no miniatura leugo de capturar una foto
    private var _chooseColorFromLastPreview:Boolean = false;      // Indica si se muestra o no pantalla de selección de color en las última toma
    private var _automaticFilter:String;                    // Indica el nombre del filtro que se aplica automaticamente al llegar a la pantalla de filtros, si está vacio o no existe, no se aplica ninguno. Debe existir un botón con este mismo nombre
    private var _automaticBackground:String;                    // Indica el nombre del backround que se aplica automaticamente al llegar a la pantalla de filtros, si está vacio o no existe, no se aplica ninguno. Debe existir un botón con este mismo nombre
    private var _automaticOverlay:String;                    // Indica el nombre del overlay que se aplica automaticamente al llegar a la pantalla de filtros, si está vacio o no existe, no se aplica ninguno. Debe existir un botón con este mismo nombre
    private var _highLightCurrentFilter:String;            // Cuando tiene un valor, se muestra esta imagen al seleccionarse un filtro
    private var _hideFrameBeforeCapturePicture:Boolean = false;    // Cuando tiene un valor, se muestra esta imagen al seleccionarse un filtro
    private var _paparazziPicturePath:Vector.<String> = new Vector.<String>();           // Indica la(s) foto(s) que fue elegida en la modalidad de paparazzi
    private var _outputModule:String;                   // El output module usado para renderizar, equivale a la extensión del archivo, y se configura en el tercerp arametro de applyFilter
    private var _isPictureDraggable:Boolean  = false;     // Indica si la imagen de la cámara puede moverse como un prop
    private var _trimAlphaEnabled:Boolean  = true;     // Indica si la imagen se debe recortar el alpha una vez que sale del AE.  (para cuando isPictureDraggable = true)
    private var _antialiasingEnabled:Boolean = false;       // inidca si se aplica o no antialiasing a picture en caso de ser draggable
    private var _isGIF:Boolean = false;                     // Determina si la imagen de salida es un gif animado o no
    private var _gifDuration:Number = 200;                     // Tiempo de animación entre cada frame del GIF.
    private var _isPaparazzi:Boolean = false;                     // Determina si estamos en modo paparazzi o no
    private var _isFaceme:Boolean = false;                     // Determina si estamos en modo faceme o no, solo funciona con modo 1picture
    private var _sendPictureFromAE:Boolean = false;             // Indica si la imagen que se envía la GUI es directamente la de salida de AE (true) o se captura de la pantalla (false)
    private var _showOverlaySelection:Boolean = false;          // Indica si se debe o no mostrar pantalla para seleccionar Overlay antes de capturar foto.





    // =========================================
    public function get params():Object
    {
        return this._params;
    }
    public function set params(_params:Object):void
    {
        this._params = _params;
    }
    // =========================================
    public function get outputModule():String
    {
        return this._outputModule;
    }
    public function set outputModule(_outputModule:String):void
    {
        this._outputModule = _outputModule;
    }
    // =========================================
    public function get paparazziPicturePath():Vector.<String>
    {
        return this._paparazziPicturePath;
    }
    // =========================================
    public function get language():String
    {
        return this._language;
    }
    public function set language(_language:String):void
    {
        this._language = _language;
    }
    // =========================================
    public function set sessionType(_sessionType:String):void
    {
        this._sessionType = _sessionType;
    }
    public function get sessionType():String
    {
        return this._sessionType;
    }
    // =========================================
    public function get selectedBackground():String
    {
        return this._selectedBackground;
    }
    public function set selectedBackground(_selectedBackground:String):void
    {
        this._selectedBackground = _selectedBackground;
    }
    // =========================================
    public function get selectedOverlay():String
    {
        return this._selectedOverlay;
    }
    public function set selectedOverlay(_selectedOverlay:String):void
    {
        this._selectedOverlay = _selectedOverlay;
    }
    // =========================================
    public function get selectedObject():Object
    {
        return this._selectedObject;
    }
    public function set selectedObject(_selectedObject:Object):void
    {
        this._selectedObject = _selectedObject;
    }
    // =========================================
    public function set jsonPath(_jsonPath:String):void
    {
        this._jsonPath = _jsonPath;
    }
    public function get jsonPath():String
    {
        return this._jsonPath;
    }
    // =========================================
    public function set automaticFilter(_automaticFilter:String):void
    {
        this._automaticFilter = _automaticFilter;
    }
    public function get automaticFilter():String
    {
        return this._automaticFilter;
    }
    // =========================================
    public function set automaticBackground(_automaticBackground:String):void
    {
        this._automaticBackground = _automaticBackground;
    }
    public function get automaticBackground():String
    {
        return this._automaticBackground;
    }
    // =========================================
    public function set automaticOverlay(_automaticOverlay:String):void
    {
        this._automaticOverlay = _automaticOverlay;
    }
    public function get automaticOverlay():String
    {
        return this._automaticOverlay;
    }
    // =========================================
    public function set photosPerSession(_photosPerSession:uint):void
    {
        this._photosPerSession = _photosPerSession;
    }
    public function get photosPerSession():uint
    {
        return this._photosPerSession;
    }
    // =========================================
    public function set photosTimePerSession(_photosTimePerSession:uint):void
    {
        this._photosTimePerSession = _photosTimePerSession;
    }
    public function get photosTimePerSession():uint
    {
        return this._photosTimePerSession;
    }
    // =========================================
    public function set previewTimeBeforeCaptureNextPicture(_previewTimeBeforeCaptureNextPicture:uint):void
    {
        this._previewTimeBeforeCaptureNextPicture = _previewTimeBeforeCaptureNextPicture;
    }
    public function get previewTimeBeforeCaptureNextPicture():uint
    {
        return this._previewTimeBeforeCaptureNextPicture;
    }
    // =========================================
    public function set showPreviewAfterCapture(_showPreviewAfterCapture:Boolean):void
    {
        this._showPreviewAfterCapture = _showPreviewAfterCapture;
    }
    public function get showPreviewAfterCapture():Boolean
    {
        return this._showPreviewAfterCapture;
    }
    // =========================================
    public function set isPictureDraggable(_isPictureDraggable:Boolean):void
    {
        this._isPictureDraggable = _isPictureDraggable;
    }
    public function get isPictureDraggable():Boolean
    {
        return this._isPictureDraggable;
    }
    // =========================================
    public function set trimAlphaEnabled(_trimAlphaEnabled:Boolean):void
    {
        this._trimAlphaEnabled = _trimAlphaEnabled;
    }
    public function get trimAlphaEnabled():Boolean
    {
        return this._trimAlphaEnabled;
    }
    // =========================================
    public function set antialiasingEnabled(_antialiasingEnabled:Boolean):void
    {
        this._antialiasingEnabled = _antialiasingEnabled;
    }
    public function get antialiasingEnabled():Boolean
    {
        return this._antialiasingEnabled;
    }
    // =========================================
    public function set chooseColorFromLastPreview(_chooseColorFromLastPreview:Boolean):void
    {
        this._chooseColorFromLastPreview = _chooseColorFromLastPreview;
    }
    public function get chooseColorFromLastPreview():Boolean
    {
        return this._chooseColorFromLastPreview;
    }
    // =========================================
    public function get highLightCurrentFilter():String
    {
        return this._highLightCurrentFilter;
    }
    public function set highLightCurrentFilter(_highLightCurrentFilter:String):void
    {
        this._highLightCurrentFilter = _highLightCurrentFilter;
    }
    // =========================================
    public function set hideFrameBeforeCapturePicture(_hideFrameBeforeCapturePicture:Boolean):void
    {
        this._hideFrameBeforeCapturePicture = _hideFrameBeforeCapturePicture;
    }
    public function get hideFrameBeforeCapturePicture():Boolean
    {
        return this._hideFrameBeforeCapturePicture;
    }
    // =========================================
    public function set isGIF(_isGIF:Boolean):void
    {
        this._isGIF = _isGIF;
    }
    public function get isGIF():Boolean
    {
        return this._isGIF;
    }
    // =========================================
    public function set gifDuration(_gifDuration:Number):void
    {
        this._gifDuration = _gifDuration;
    }
    public function get gifDuration():Number
    {
        return this._gifDuration;
    }
    // =========================================
    public function set isPaparazzi(_isPaparazzi:Boolean):void
    {
        this._isPaparazzi = _isPaparazzi;
    }
    public function get isPaparazzi():Boolean
    {
        return this._isPaparazzi;
    }
    // =========================================
    public function set isFaceme(_isFaceme:Boolean):void
    {
        this._isFaceme = _isFaceme;
    }
    public function get isFaceme():Boolean
    {
        return this._isFaceme;
    }
    // =========================================
    public function set sendPictureFromAE(_sendPictureFromAE:Boolean):void
    {
        this._sendPictureFromAE = _sendPictureFromAE;
    }
    public function get sendPictureFromAE():Boolean
    {
        return this._sendPictureFromAE;
    }
    // =========================================
    public function set showOverlaySelection(_showOverlaySelection:Boolean):void
    {
        this._showOverlaySelection = _showOverlaySelection;
    }
    public function get showOverlaySelection():Boolean
    {
        return this._showOverlaySelection;
    }

}
}
