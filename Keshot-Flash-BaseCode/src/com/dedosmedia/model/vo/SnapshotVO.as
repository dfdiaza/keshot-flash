/**
 * Created by dedosmedia on 27/12/16.
 */
package com.dedosmedia.model.vo {

import flash.display.BitmapData;

public class SnapshotVO {

    private var _snapshots:Vector.<BitmapData> = new Vector.<BitmapData>();

    public function addSnapshot(bmd:BitmapData):void
    {
        this._snapshots.push(bmd);
    }

    public function getSnapshot(index:uint):BitmapData
    {
        if(this._snapshots[index])
            return this._snapshots[index]
        return null;
    }

    public function clear():void
    {
        for(var i:uint = 0, l:uint = this._snapshots.length; i<l; i++ )
        {
            this._snapshots[i].dispose();
            this._snapshots[i] = null;
        }

        while(this._snapshots.length>0)
        {
            this._snapshots.pop();
        }
    }

    public function get length():Number
    {
        return this._snapshots.length;
    }

    public function dispose():void
    {
        this.clear();
    }

}
}
