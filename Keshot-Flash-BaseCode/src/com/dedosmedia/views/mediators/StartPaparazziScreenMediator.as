package com.dedosmedia.views.mediators
{

import com.dedosmedia.component.Image;
import com.dedosmedia.component.PixelViewer;
import com.dedosmedia.events.AppEventType;
import com.dedosmedia.events.ErrorEventType;
import com.dedosmedia.events.SoundEventType;
import by.blooddy.crypto.Base64;
import feathers.controls.Button;

import flash.display.BitmapData;

import flash.geom.Point;
import flash.geom.Rectangle;
import flash.system.Capabilities;

import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;
import starling.display.Sprite;

import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.utils.Pool;

import flash.display.BitmapData;

import flash.display.JPEGEncoderOptions;
import flash.external.ExternalInterface;
import flash.utils.ByteArray;
import starling.core.Starling;

import starling.events.Event;
import starling.textures.Texture;

import starling.utils.StringUtil;
import starling.utils.SystemUtil;

public class StartPaparazziScreenMediator extends CustomMediator
{

    // Texturas en miniatura que hicimos de las fotos
    private var _previewTexture:Vector.<Texture> = new Vector.<Texture>();

    // Numero de la foto que se va a capturar en la secuencia actual
    private var currentPhotoNumber:uint = 0;

    // Temp picture in ByteArray
    private var tempByteArray:ByteArray = new ByteArray();

    // Variables referentes a elementos agregados en la vista (screen)


    private var _preview:DisplayObject;
    private var _pixelTitle:DisplayObject;
    private var _pixelViewer:DisplayObjectContainer;
    private var _nextButton:DisplayObject;

    override public function onRegister():void
    {
        trace("onRegister StartPaparazziScreenMediator ")
        super.onRegister();


        this._preview = getChild("preview") as DisplayObject;
        this._pixelTitle = getChild("pixel-title") as DisplayObject;
        this._pixelViewer = getChild("pixel-viewer") as DisplayObjectContainer;
        this._nextButton = getChild("next-button") as DisplayObject;
        //Image(this._preview).scaleContent = false;
        //this._holder.addChild(this._preview);
    }

    override public function  initialize():void
    {
        super.initialize();

        Starling.current.skipUnchangedFrames = false;

        // Eliminamos cualquier snapshot capturado de una session previa
        appModel.snapshot.clear();


        if(appModel.isIDE)
        {
            appModel.config.paparazziPicturePath.push("/Volumes/OSX_USB/Dropbox (Personal)/DedosMedia/keshot/Keshot-PhotoBooth/GS Photos/Fotos/IMG_3858.JPG");
            appModel.config.paparazziPicturePath.push("/Volumes/OSX_USB/Dropbox (Personal)/DedosMedia/keshot/Keshot-PhotoBooth/GS Photos/Fotos/IMG_3859.JPG");
            appModel.config.paparazziPicturePath.push("/Volumes/OSX_USB/Dropbox (Personal)/DedosMedia/keshot/Keshot-PhotoBooth/GS Photos/Fotos/IMG_3860.JPG");
            appModel.config.paparazziPicturePath.push("/Volumes/OSX_USB/Dropbox (Personal)/DedosMedia/keshot/Keshot-PhotoBooth/GS Photos/Fotos/IMG_3861.JPG");
        }

        appModel.localConnection.addEventListener(AppEventType.APP_ERROR_LOCAL_CONNECTION,onLocalConnectionError);

        trace("Show Paparazzi Picture", appModel.config.paparazziPicturePath[0]);

        // cargamos un snapshot desde disco
        Image(this._preview).saveBitmapData = true;
        Image(this._preview).source = appModel.config.paparazziPicturePath[0];


    }

    private function onLocalConnectionError(event:Event):void {
        trace("ERROR EN LOCAL CONNECTION... NO SE PUDO COPIAR EL ARCHIVO");
        this.dispatchWith(ErrorEventType.ERROR,true,{text:"Error en llamada a LocalConnection. Verifica que DedosMeia-Keshot está siendo ejecutada", error:"LocalConnection"})

    }


    override protected function image_completeHandler(event:Event):void
    {
        super.image_completeHandler(event);
        switch (Image(event.currentTarget).name)
        {
            case "preview":
                var colorBoundsRect:Rectangle = Image(event.currentTarget).bmd.getColorBoundsRect(0xFF000000, 0x00000000, false);
                saveVideoFrame(Image(event.currentTarget).bmd);
                break;
        }
        trace("IMAGE COMPLETE LOADED ",event);

    }


    override protected function image_errorHandler(event:Event):void
    {
        super.image_errorHandler(event);
        trace("IMAGE ERRRO LOADING ",event);
    }

    protected function saveVideoFrame(bmd:BitmapData):void {


        _previewTexture.push(Texture.fromBitmapData(bmd))


        if (ExternalInterface.available) {



            for(var i=0, l= appModel.config.paparazziPicturePath.length; i<l; i++)
            {
                trace("copyFile: ",appModel.config.paparazziPicturePath[i], StringUtil.format("input{0}.jpg",currentPhotoNumber));
                appModel.inputFilesPath.push(appModel.config.paparazziPicturePath[i]);
                var path:String = ExternalInterface.call("copyFile", appModel.config.paparazziPicturePath[i],StringUtil.format("input{0}.jpg",currentPhotoNumber++), "input");
            }

            appModel.snapshot_path = path;
            trace("Picture saved to:",appModel.snapshot_path)

        }
        else
        {
            // SOLO EN DEBUG
            appModel.snapshot_path = "C:";
            trace("Picture simulated to be saved to: ",appModel.snapshot_path )
        }

/*
        var dstPath:String = StringUtil.format("temp/input{0}.jpg",currentPhotoNumber);
        appModel.localConnection.send("app#com.dedosmedia.Keshot","echo",{"srcPath":appModel.config.paparazziPicturePath[0], "projectPath":appModel.url, "dstPath":dstPath})
        appModel.localConnection.send("app#com.dedosmedia.Keshot","copyFile",{"srcPath":appModel.config.paparazziPicturePath[0], "projectPath":appModel.url, "dstPath":dstPath})
*/

        this.showNextScreen();

    }

    private function showNextScreen():void
    {
        // decidir si permitimos o no seleccionar punto de color
        if(appModel.config.chooseColorFromLastPreview)
        {
            Starling.current.skipUnchangedFrames = true;
            Image(this._preview).source = _previewTexture[_previewTexture.length-1];
            Image(this._preview).visible = true;
            if(this._pixelTitle)
                Image(this._pixelTitle).visible = true;

            Button(this._nextButton).visible = true;
            PixelViewer(this._pixelViewer).texture = _previewTexture[_previewTexture.length-1];
        }
        else
        {
            this.dispatchWith(AppEventType.APP_NEXT_SCREEN,true,{screen:screen});
        }

    }


    override protected function image_touchHandler(event:TouchEvent):void
    {
        super.image_touchHandler(event);

        switch(DisplayObject(event.target).name)
        {
            case "preview":



                var touches = event.getTouches(Image(event.target));
                if(touches.length == 1 && (touches[0].phase == TouchPhase.MOVED || touches[0].phase == TouchPhase.BEGAN)) {


                    this._pixelViewer.visible = true;

                    var point:Point = touches[0].getLocation(Image(event.target).internalImage, Pool.getPoint())

                    //var pointtouches[0].getLocation(DisplayObject(Image(event.target).internalImage)), "point outside",point)
                    PixelViewer(this._pixelViewer).showPoint(point);
                    Pool.putPoint(point);
                }
                break;
            default:
                trace("ALGUIEN MAS ", Image(event.target).parent.name)
        }



    }

    override protected function button_triggeredHandler(e:Event):void
    {
        super.button_triggeredHandler(e);

        switch(Button(e.currentTarget).name)
        {
            case "next-button":
                if(PixelViewer(this._pixelViewer).pixelSelected)
                {
                    var bmd:BitmapData = PixelViewer(this._pixelViewer).getPixelColor();

                    if (ExternalInterface.available) {
                        tempByteArray.clear()
                        bmd.encode(bmd.rect,new JPEGEncoderOptions(),tempByteArray);
                        var path:String = ExternalInterface.call("saveTempImage", Base64.encode(tempByteArray),StringUtil.format("color.jpg",currentPhotoNumber), "input");
                        appModel.inputFilesPath.push(path);
                        trace("Color saved to: ",path );


                    }
                    else
                    {
                        // SOLO EN DEBUG
                        trace("Color simulated to be saved ", bmd.rect);
                    }
                    bmd.dispose();
                }

                // Guardar la imagen de color, si han elegido
                this.dispatchWith(AppEventType.APP_NEXT_SCREEN,true,{screen:screen});
                break;
        }
    }


    override public function onRemove():void
    {
        if(_previewTexture)
        {
            _previewTexture.forEach(function(item:Texture, index:int, vector:Vector.<Texture>):void{
                item.dispose();
            })
            _previewTexture = null;
        }
        appModel.localConnection.removeEventListener(AppEventType.APP_ERROR_LOCAL_CONNECTION,onLocalConnectionError);

        trace("onRemove::StartPaparazziScreenMediator")
        super.onRemove();
    }

}
}