package com.dedosmedia.views.mediators
{
import com.dedosmedia.events.AppEventType;
import com.dedosmedia.events.ErrorEventType;
    import com.dedosmedia.events.FileEventType;
    import com.dedosmedia.events.NativeProcessEventType;
    import com.dedosmedia.model.AppModel;
	import com.dedosmedia.model.AssetsModel;
    import com.dedosmedia.utils.TwoWayLocalConnection;
    import com.dedosmedia.views.components.FilterScreen;
import com.dedosmedia.views.components.OverlayScreen;
import com.dedosmedia.views.components.StartFacemeScreen;
import com.dedosmedia.views.components.StartPaparazziScreen;
import com.dedosmedia.views.components.StartScreen;

import feathers.controls.ImageLoader;


import flash.display.Loader;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
import flash.external.ExternalInterface;
import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	import feathers.controls.Alert;
	import feathers.controls.StackScreenNavigatorItem;
	import feathers.data.ListCollection;
	import feathers.motion.Slide;

	import org.robotlegs.starling.mvcs.Mediator;
	
	import starling.core.Starling;
import starling.display.Image;
import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	import starling.utils.StringUtil;
    import starling.utils.SystemUtil;

    public class MainAppMediator extends Mediator
	{
		
		public var screen:MainApp
		
		
		[Inject]
		public var appModel:AppModel; 
		
		[Inject]
		public var assetsModel:AssetsModel; 

		///---- NEW

		private var availableAspectRatio:Array = new Array();		// Lista de carpetas de contenido con los aspect ratios disponibles
		private var availableScale:Array = new Array();				// Lista de carpetas de contenido con las resoluciones disponibles dentro determinado aspect ratio
		private var factor: Number = 1;								// Factor de escala a usar para el AssetManager
		
		
		private var currentAspectRatio:Number;						// Relación de aspecto actual de nuestra pantalla
		private var assetsPath:String = "";								// Apunta a la carpeta donde estan los assets segun el mejor aspect ratio y resolución para la pantalla actual
		private var assetsConfig:Object								// Contiene el json de configuracion generico de las texturas (indica a q resolucion se diseñó la interfaz)
		
		
		
		override public function onRegister():void {
            super.onRegister();

            screen = this.getViewComponent() as MainApp;
            trace(" onRegister::MainAppMediator.as")


            trace("DRIVER")
            trace(Starling.current.context.driverInfo, Starling.context.driverInfo)

            appModel.localConnection = new TwoWayLocalConnection("SWF", "AIR", this)
            appModel.size = new Rectangle(0, 0, Starling.current.stage.stageWidth, Starling.current.stage.stageHeight);
            appModel.url = screen.url;


            //this.addContextListener(FileEventType.FILE_READ_COMPLETED, updateConfigVO, Event);
            //this.dispatchWith(FileEventType.FILE_READ_JSON_START, true, {file: StringUtil.format("content/json/{0}/config.json",appModel.config.sessionType)})



            this.addContextListener(FileEventType.FILE_READ_COMPLETED, globalConfig, Event);
            this.dispatchWith(FileEventType.FILE_READ_JSON_START, true, {file: StringUtil.format("content/json/config.json",appModel.config.sessionType)})

            trace("ADD CALLBACK")
            //Esta porción de código quiebra la aplicación, y hace que siempre se reciba null desde una llamada a JavaScript
            if (ExternalInterface.available) {
                try
                {
                    ExternalInterface.addCallback("cleanFlash", cleanFlash);
                    ExternalInterface.addCallback("restartFlash", restartFlash);
                }
                catch (error:Error)
                {
                    trace("Error ",error.getStackTrace())
                }
            }
        }

        private function cleanFlash():void
        {
            trace(" ============>  CLEAN FLASH");
            appModel.localConnection.close();
        }

        private function restartFlash():void
        {
            trace("================> RESTART FLASH");
        }
        private function globalConfig(event:Event):void
        {
            this.removeContextListener(FileEventType.FILE_READ_COMPLETED, globalConfig, Event);


            trace("GLOBAL CONFIG::");

            for(var i in event.data)
            {
                trace(i+" -> "+event.data[i]);
            }



            // Si es simulado, no hay params

            appModel.config.sessionType     =   screen.params.flashSessionTypes || screen.initConfig.config.sessionType;
            appModel.config.language        =   screen.params.flashSessionTypes || screen.initConfig.config.language;





            // Aqui se actualiza el sessionTYpe pasado por el HTML, al elegido
            //appModel.config.jsonPath = event.data.data[screen.params.flashSessionTypes];
            appModel.config.jsonPath = event.data.data[appModel.config.sessionType];

            trace(event.data.data, appModel.config.sessionType)
            trace("PARAMS: ",screen.params,appModel.config.sessionType,appModel.config.jsonPath);

            this.addContextListener(FileEventType.FILE_READ_COMPLETED, updateConfigVO, Event);
            this.dispatchWith(FileEventType.FILE_READ_JSON_START, true, {file: StringUtil.format("content/json/{0}/config.json",appModel.config.jsonPath)})


        }
        private function updateConfigVO(event:Event):void
        {
            trace("LOACL CONFIG LOADED");
            for(var i in event.data)
            {
                trace(i+" -> "+event.data[i]);
            }


            this.removeContextListener(FileEventType.FILE_READ_COMPLETED, updateConfigVO, Event);

            this.addContextListener(AppEventType.APP_CONFIGVO_UPDATED, configStackNavigator)
            this.dispatchWith(AppEventType.APP_CONFIGVO_START, true, {"params":screen.params, "config":event.data.data})



            // Reemplazado por UpdateConfigVOCommand

            //appModel.config.sessionType     =   screen.initConfig.config.sessionType    ||  data.sessionType;
            //appModel.config.language        =   screen.initConfig.config.language       ||  data.language;

            //configStackNavigator();

        }

        private function configStackNavigator():void
        {
            this.removeContextListener(AppEventType.APP_CONFIGVO_UPDATED, configStackNavigator);


            /*
             *  OVERLAY
             */

            var overlayItem:StackScreenNavigatorItem = new StackScreenNavigatorItem( OverlayScreen );
            overlayItem.setScreenIDForPushEvent(MainApp.SHOW_START,MainApp.START);
            overlayItem.setScreenIDForPushEvent(MainApp.SHOW_START_PAPARAZZI,MainApp.START_PAPARAZZI);
            overlayItem.setScreenIDForPushEvent(MainApp.SHOW_START_FACEME,MainApp.START_FACEME);
            screen.addScreen( MainApp.OVERLAY, overlayItem );


			/*
			*  START
			*/

			var startItem:StackScreenNavigatorItem = new StackScreenNavigatorItem( StartScreen );
            startItem.setScreenIDForPushEvent(MainApp.SHOW_FILTER,MainApp.FILTER);
			screen.addScreen( MainApp.START, startItem );


            /*
             *  START PAPARAZZI
             */

            var startPaparazziItem:StackScreenNavigatorItem = new StackScreenNavigatorItem( StartPaparazziScreen );
            startPaparazziItem.setScreenIDForPushEvent(MainApp.SHOW_FILTER,MainApp.FILTER);
            screen.addScreen( MainApp.START_PAPARAZZI, startPaparazziItem );

            /*
             *  START FACEME
             */

            var startFacemeItem:StackScreenNavigatorItem = new StackScreenNavigatorItem( StartFacemeScreen );
            startFacemeItem.setScreenIDForPushEvent(MainApp.SHOW_FILTER,MainApp.FILTER);
            screen.addScreen( MainApp.START_FACEME, startFacemeItem );

			/*
			 *  FILTER
			 */

            var filterItem:StackScreenNavigatorItem = new StackScreenNavigatorItem( FilterScreen );
            filterItem.addPopEvent(Event.CLOSE);
            screen.addScreen( MainApp.FILTER, filterItem );




			screen.pushTransition = Slide.createSlideLeftTransition();
			screen.popTransition = Slide.createSlideDownTransition()

			this.addContextListener(ErrorEventType.ERROR, handlerError,Event);


			Starling.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKey);

			appModel.os = SystemUtil.platform;

			// INTENTAR DESCARGAR PROYECTOS DE INTERNET!!!!!

			trace("OS: "+appModel.os);

            // Revisamos las resolucion disponibles para ver cual es la mas ajustada

            currentAspectRatio = Starling.current.nativeStage.stage.fullScreenWidth / Starling.current.nativeStage.stage.fullScreenHeight;

            // crea un objeto con los aspect ratios disponibles

            trace("Screen ",screen)
            trace("Screen1 ",screen.initConfig)
            trace("Screen2 ",screen.initConfig.config)
            trace("Screen3 ",screen.initConfig.config.available_aspect)


            for (var i:uint = 0, l:uint = screen.initConfig.config.available_aspect.length ; i < l; i++)
            {
                var name:String = screen.initConfig.config.available_aspect[i].aspect;
                var design_size:Object = screen.initConfig.config.available_aspect[i].design_size;
                var temp	:Array 		= name.split("x");
                var w		:Number 	= Number(temp[0])
                var h		:Number 	= Number(temp[1])
                var ratio	:Number	 	= w/h;
                // solo folder cuyo nombre cumpla la regla wxh en su nombre (donde w y h deben ser numeros enteros)
                if(!isNaN(ratio))
                {
					trace("Available aspect: ",name,w/h);
                    availableAspectRatio.push({aspect:name, ratio:w/h, item:screen.initConfig.config.available_aspect[i]})
                }
            }

            //buscar el folder con la relacion de aspecto màs cercana a la que tiene nuestra máquina
            var index: uint = nearestNumber(currentAspectRatio,availableAspectRatio)
            assetsPath = availableAspectRatio[index].aspect

            trace("--Path for best Aspect Ratio: "+assetsPath)

            try
            {
                trace("-- Resolución del diseño: ",availableAspectRatio[index].item.design_size.width, availableAspectRatio[index].item.design_size.height);
                appModel.design_size = new Rectangle(0,0,availableAspectRatio[index].item.design_size.width, availableAspectRatio[index].item.design_size.height);


                // para el aspect ratio seleccionado... busca las carpetas y calcula la escala de cada una de ellas.
                for (i = 0, l = availableAspectRatio[index].item.available_resolution.length; i < l; i++)
                {

                    // El ratio se calcula teniendo en cuenta unicamente el ancho
                    var w:Number = availableAspectRatio[index].item.available_resolution[i].width;
                    var h:Number = availableAspectRatio[index].item.available_resolution[i].height
                    var ratio:Number = w/Starling.current.stage.stageWidth;
                    availableScale.push({name:w+"x"+h, ratio:ratio, w:w, h:h})

                    trace("Available resolution:: "+w+"x"+h+", ratio:",ratio);
                }

                // Buscar el zoom más cercano al contentScaleFactor
                index = nearestNumber(Starling.current.contentScaleFactor,availableScale)
                factor = availableScale[index].ratio;
                trace("-- Path for best resolution: "+availableScale[index].name,factor)
                trace("-- VP:"+Starling.current.viewPort,availableScale[index].w,availableScale[index].h)




                var stageSize:Rectangle  = new Rectangle(0, 0, availableScale[index].w, availableScale[index].h)	//  Que sea la mejor resolución escogida
                var screenSize:Rectangle = new Rectangle(0, 0, Starling.current.stage.stageWidth, Starling.current.stage.stageHeight); 	//  la resolución de nuestra pantalla
                var viewPort:Rectangle = RectangleUtil.fit(stageSize, screenSize, ScaleMode.SHOW_ALL);

                trace("available resolution:",stageSize,"screenSize",screenSize,"viewport",viewPort)

                Starling.current.viewPort = viewPort;

                Starling.current.stage.stageWidth = stageSize.width///viewPort.width;
                Starling.current.stage.stageHeight = stageSize.height///viewPort.height;

                trace("COMPARISON::: ",appModel.design_size.width,availableScale[index].w)

                // Este es el factor real entre el diseño, y el tamaño en pixeles en q se ve el view port (redimensionado a ocupar la mayor parte de pantalla)
                factor = appModel.design_size.width/viewPort.width;
                trace("NEW FACTOR "+factor)
                trace("CSF: "+Starling.current.contentScaleFactor)

                assetsPath = assetsPath+"/"+availableScale[index].w+"x"+availableScale[index].h;
                trace("assetsPath ",assetsPath)
                launchProject();
            }
            catch(e:SyntaxError)
            {
                trace("ERROR: Syntax error. JSON")
            }



            //launchProject();
        }



        private function launchProject():void
        {
            appModel.projectDirectory = "content/";




            assetsModel.assets.enqueue(appModel.projectDirectory+StringUtil.format("json/{0}/loading.json",appModel.config.jsonPath));




            /*
            assetsModel.assets.enqueue(StringUtil.format(appModel.projectDirectory+"audio/{0}/TakePictures.mp3",appModel.config.language));
            assetsModel.assets.enqueue(StringUtil.format(appModel.projectDirectory+"audio/{0}/SelectFromOptionsBelow.mp3",appModel.config.language));
            assetsModel.assets.enqueue(StringUtil.format(appModel.projectDirectory+"audio/{0}/SelectPictures.mp3",appModel.config.language));
            assetsModel.assets.enqueue(StringUtil.format(appModel.projectDirectory+"audio/{0}/TakePictures_Count.mp3",appModel.config.language));
            assetsModel.assets.enqueue(StringUtil.format(appModel.projectDirectory+"text/{0}/text.json",appModel.config.language));


            assetsModel.assets.enqueue(appModel.projectDirectory+"json/config.json");


            assetsModel.assets.enqueue(appModel.projectDirectory+"image/screen.png");
            //assetsModel.assets.enqueue(appModel.projectDirectory.resolvePath("fonts"));
            */

            assetsModel.assets.addEventListener(starling.events.Event.PARSE_ERROR, onParseError);
            assetsModel.assets.loadQueue(queueProgress);


        }

        private function onParseError(e:starling.events.Event):void
        {
            trace("XXXXXXXXXXXXXXXXX  ERROR MAL CONFIGURACION DE FICHERO JSON... NO SE PUEDE DECODIFICAR.  XXXXXXXXXXXXXX - ", e.data)
        }

        private function queueProgress(ratio:Number):void
        {
            trace("Loading assets XML, progress:", ratio);
            if (ratio == 1.0)
            {



                currentAspectRatio = Starling.current.nativeStage.stage.fullScreenWidth / Starling.current.nativeStage.stage.fullScreenHeight;
                trace("Current Aspect Ration: "+currentAspectRatio)

                // preload content indicated by loading.json

                var loading:Object = assetsModel.assets.getObject("loading");
                var l:uint = 0;
                var localized:String = appModel.config.language;
                var resolution:String = ""
                var subpath:String = "";
                var name:String = "";
                var mipmap:Boolean = false;



                var files:Array = loading.type["image"].files;
                var preloadFiles:Array = files.filter(isPreload)


                l = preloadFiles.length;
                subpath = loading.type["image"].path;
                for(var j:uint = 0 ; j < l; j++)
                {
                    localized = preloadFiles[j].localized == true?appModel.config.language+"/":"";
                    resolution = preloadFiles[j].resolutionDependent == true ? assetsPath+"/":"";

                    mipmap = preloadFiles[j].isProp == true ? true:false;
                    name = preloadFiles[j].name;



                    assetsModel.assets.scaleFactor = 1;
                    assetsModel.assets.useMipMaps = mipmap;
                    assetsModel.assets.enqueue(appModel.projectDirectory+subpath+resolution+localized+name);
                }
                //assetsModel.assets.loadQueue(finalQueueProgress);
                assetsModel.assets.loadQueue(preloadQueueProgress);
            }
        }
        function isPreload(element:*, index:int, arr:Array):Object
        {
            return (element.preload == true);
        }

        private function preloadQueueProgress(ratio:Number):void {
            trace("**** Preload assets, progress:", ratio);
            if (ratio == 1.0) {
                trace("done preload")
                screen.addChild(new Image(assetsModel.assets.getTexture("screen1")));

                // Cargar resto de assets
                var loading:Object = assetsModel.assets.getObject("loading");
                var l:uint = 0;
                var localized:String = appModel.config.language;
                var resolution:String = ""
                var subpath:String = "";
                var name:String = "";
                var mipmap:Boolean = false;
                var sessionTypeDependent:String;
                var dynamicLoading:Boolean = false;

                for( var i:String in loading.type)
                {
                    trace("i",i,loading.type[i]);
                    l = loading.type[i].files.length;
                    subpath = loading.type[i].path;
                    for(var j:uint = 0 ; j < l; j++)
                    {
                        // saltar los que ya fueron precargdos
                        if(loading.type[i].files[j].preload==true)
                            continue;

                        localized = loading.type[i].files[j].localized == true?appModel.config.language+"/":"";
                        resolution = loading.type[i].files[j].resolutionDependent == true ? assetsPath+"/":"";
                        sessionTypeDependent = loading.type[i].files[j].sessionTypeDependent == true ? appModel.config.jsonPath+"/":"";
                        mipmap = loading.type[i].files[j].mipmap == true ? true:false;
                        name = loading.type[i].files[j].name;
                        dynamicLoading = loading.type[i].files[j].dynamicLoading == true ? true:false;

                        if(dynamicLoading)
                        {
                            trace("selectedBackground ",appModel.config.params.selectedBackground);
                            name = StringUtil.format(name,appModel.config.selectedBackground);
                            trace("This is the dynamic background to load: ",name);
                        }


                        // ex: content/audio/en/audio.mp3
                        assetsModel.assets.scaleFactor = 1;
                        assetsModel.assets.useMipMaps = mipmap;
                        trace("SessionTypeDependant: ",sessionTypeDependent);
                        //assetsModel.assets.enqueue(appModel.projectDirectory+subpath+resolution+localized+name);
                        trace("LOAD::::: "+appModel.projectDirectory+subpath+sessionTypeDependent+resolution+localized+name)
                        assetsModel.assets.enqueue(appModel.projectDirectory+subpath+sessionTypeDependent+resolution+localized+name);
                    }
                }




                assetsModel.assets.loadQueue(finalQueueProgress);
            }
        }

        private function finalQueueProgress(ratio:Number):void
        {
            trace("**** Loading assets, progress:", ratio);
            if (ratio == 1.0)
            {
                trace("done ", assetsModel.assets.getURL("screen1"));


                while(screen.numChildren>0)
                {
                    screen.removeChildAt(0,true);
                }


                trace("isPaparazzi",appModel.config.isPaparazzi);
                trace("isFaceme",appModel.config.isFaceme);

                if(appModel.config.showOverlaySelection)
                {
                    screen.rootScreenID = MainApp.OVERLAY;
                }
                else
                {
                    if(appModel.config.isPaparazzi)
                    {
                        screen.rootScreenID = MainApp.START_PAPARAZZI;
                    }
                    else if (appModel.config.isFaceme)
                    {
                        screen.rootScreenID = MainApp.START_FACEME;
                    }
                    else
                    {
                        trace("NO ES NADA")
                        screen.rootScreenID = MainApp.START;
                    }
                }






            }
        }


        /*
        * METODOS LLAMADOS POR EL LOCALCONNECTION
        *
        * */
        public function echo(obj:Object):void {
            trace("ECHO RECIBIDO EN FLASH.")
            for(var i in obj)
            {
                trace(i,obj[i])

            }
        }


        public function receiveMessage(obj:Object):void {
			try{
				trace("RECIBO MENSAJE EN FLASH-. RENDERIZADO COMPLETO")
                for(var i in obj)
                 trace(i,obj[i])
                this.dispatchWith(NativeProcessEventType.NATIVEPROCESS_COMPLETED)
            }
			catch (e:Error)
			{
				// Si hay una excepcion... parece q no se ejecutara la llamada al a funcion
				trace("CATCH ERROR",e.getStackTrace());
			}
    	}

		
		//Muestra todos los errores en el software
		private function handlerError(e:starling.events.Event):void
		{
            /*
			Alert.show(e.data.text+" \n "+e.data.error,"Error",  new ListCollection(
				[
					{ label: "OK", triggered: function():void{trace("cerrar");} }
				]));
				*/
		}
		
		private function onKey(event:KeyboardEvent, key:*):void
		{
			// flash native stage
			var _stage:Stage = Starling.current.nativeStage;
			if(event.ctrlKey)
			{
				switch(key)
				{
					case Keyboard.H:
						
						if(_stage.displayState == StageDisplayState.NORMAL)
						{
							_stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
						}
						else
						{
							_stage.displayState = StageDisplayState.NORMAL;
						}
						break;
					case Keyboard.X:
						Alert.show("¿Estás seguro que quieres cerrar la aplicación?","Confirmación",new ListCollection([{label:"Cerrar", triggered:function(){trace("CLose")}}, {label:"Cancelar"}]));
						break;
				}
			}
		}
		


		// ontiene el folder con la resolucion mas cercana a la del equipo
		public function nearestNumber(value:Number,list:Array):uint {
			var currentNumber:Number = list[0].ratio;
			var index:uint = 0;
			for (var i:int = 0; i < list.length; i++) {
				if (Math.abs(value - list[i].ratio) < Math.abs(value - currentNumber)){
					currentNumber = list[i].ratio;
					index = i;
				}
			}
			return index; 
		}
		
		
		private function initStarling():void
		{
			trace("Starling created: ")

			var stageSize:Rectangle  = new Rectangle(0, 0, Starling.current.stage.stageWidth, Starling.current.stage.stageHeight)	//  Que sea la mejor resolución escogida 
			var screenSize:Rectangle = new Rectangle(0, 0, Starling.current.stage.stageWidth, Starling.current.stage.stageHeight); 	//  la resolución de nuestra pantalla
			var viewPort:Rectangle = RectangleUtil.fit(stageSize, screenSize, ScaleMode.SHOW_ALL);
			
			Starling.current.viewPort = viewPort;
	
			Starling.current.antiAliasing = assetsConfig.hasOwnProperty("antiAliasing")?assetsConfig["antiAliasing"].value:0;
			Starling.current.stage.stageWidth = Starling.current.stage.stageWidth;
			Starling.current.stage.stageHeight = Starling.current.stage.stageHeight;

			
			if(Starling.current.showStats == true)
			{
				Starling.current.showStatsAt("left","bottom",stageSize.width/screenSize.width);
			}
			
			
			trace("stageSize: "+stageSize)
			trace("screenSize: "+screenSize)
			trace("VP: "+viewPort)
			trace("Star Size "+Starling.current.stage.stageWidth,Starling.current.stage.stageHeight)
			trace("scale "+Starling.current.contentScaleFactor)




		}

		

	}
}