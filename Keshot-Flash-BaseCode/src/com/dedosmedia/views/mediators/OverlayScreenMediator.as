package com.dedosmedia.views.mediators
{

import com.dedosmedia.component.CameraTexture;
import com.dedosmedia.component.CameraTexture2;
import com.dedosmedia.component.ICameraTexture;
import com.dedosmedia.component.Image;
import com.dedosmedia.component.PixelViewer;
import com.dedosmedia.component.PixelViewer;
import com.dedosmedia.events.AppEventType;
import com.dedosmedia.events.SoundEventType;
import by.blooddy.crypto.Base64;
import com.dedosmedia.component.Button;


import flash.geom.Point;


import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;

import starling.display.Sprite;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.utils.Color;
import starling.utils.Pool;

//import feathers.controls.ImageLoader;
import flash.display.BitmapData;

import flash.display.JPEGEncoderOptions;
import flash.external.ExternalInterface;
import flash.utils.ByteArray;
import starling.core.Starling;

import starling.display.MovieClip;
import starling.events.Event;
import starling.textures.Texture;

import starling.utils.StringUtil;
	
	public class OverlayScreenMediator extends CustomMediator
	{


		
		override public function onRegister():void
		{
			trace("onRegister OverlayScreenMediator ")
			super.onRegister();


		}

        override public function  initialize():void
        {
            super.initialize();

            Starling.current.skipUnchangedFrames = true;



        }


        override protected function button_triggeredHandler(e:Event):void {
            super.button_triggeredHandler(e);

            trace("ARGS ", Button(e.currentTarget).args);

            if (Button(e.currentTarget).callback != "") {
                try {
                    trace("buuton: ", Button(e.currentTarget).callback)
                    this[Button(e.currentTarget).callback].apply(Button(e.currentTarget), Button(e.currentTarget).args);
                }
                catch (e:Error) {
                    trace("ERROR: Button  callback. ", e.getStackTrace());
                }
            }
        }

        // El scope de esta función es el botón presionado, así que this se refiere al botón
        private var applyObject:Function = function(settings:Object):void
        {
            trace("applyObject function : ", settings);
            appModel.config.selectedObject = settings;
            showNextScreen();
        }



        private function showNextScreen():void
        {
            trace("Dispatch Next")
            this.dispatchWith(AppEventType.APP_NEXT_SCREEN,true,{screen:screen});
        }





		override public function onRemove():void
		{

			trace("onRemove::OverlayScreenMediator")
			super.onRemove();
		}

	}
}