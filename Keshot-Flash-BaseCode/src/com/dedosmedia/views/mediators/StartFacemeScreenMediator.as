package com.dedosmedia.views.mediators
{


import com.dedosmedia.component.CameraTexture;
import com.dedosmedia.component.CameraTexture2;
import com.dedosmedia.component.ICameraTexture;
import com.dedosmedia.component.Image;
import com.dedosmedia.component.PixelViewer;
import com.dedosmedia.component.PixelViewer;
import com.dedosmedia.events.AppEventType;
import com.dedosmedia.events.SoundEventType;
import by.blooddy.crypto.Base64;
import feathers.controls.Button;


import flash.geom.Point;


import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;

import starling.display.Sprite;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.utils.Color;
import starling.utils.Pool;

//import feathers.controls.ImageLoader;
import flash.display.BitmapData;

import flash.display.JPEGEncoderOptions;
import flash.external.ExternalInterface;
import flash.utils.ByteArray;
import starling.core.Starling;

import starling.display.MovieClip;
import starling.events.Event;
import starling.textures.Texture;

import starling.utils.StringUtil;
	
	public class StartFacemeScreenMediator extends CustomMediator
	{

        // Texturas en miniatura que hicimos de las fotos
        private var _previewTexture:Vector.<Texture> = new Vector.<Texture>();

        // Numero de la foto que se va a capturar en la secuencia actual
        private var currentPhotoNumber:uint = 0;

        // Temp picture in ByteArray
        private var tempByteArray:ByteArray = new ByteArray();

        // Variables referentes a elementos agregados en la vista (screen)
        private var _video:DisplayObject;
        private var _timer:MovieClip;
        private var _colorChoose:DisplayObjectContainer;
        private var _preview:DisplayObject;
        private var _pixelTitle:DisplayObject;
        private var _pixelViewer:DisplayObjectContainer;
        private var _nextButton:DisplayObject;
        private var _captureButton:DisplayObject;

        private var _reference:DisplayObject;

		override public function onRegister():void
		{
			trace("onRegister StartFacemeScreenMediator ")
			super.onRegister();



            this._timer = getChild("timer") as MovieClip;
            this._colorChoose = getChild("color-choose") as DisplayObjectContainer;
            this._video = getChild("video") as DisplayObject;
            this._preview = getChild("preview") as DisplayObject;
            this._pixelTitle = getChild("pixel-title") as DisplayObject;
            this._pixelViewer = getChild("pixel-viewer") as DisplayObjectContainer;
            this._nextButton = getChild("next-button") as DisplayObject;
            this._captureButton = getChild("capture-button") as DisplayObject;
            this._reference = getChild("reference") as DisplayObject;


            // el numero de hole, en selectedObject determina el número de tomas que debe hacer la webcam
            if(appModel.config.selectedObject && appModel.config.selectedObject.hole)
            {
                // hole es un string o un array? por compatibilidad con configuracioens anteriores que eran string
                var hole:String = "";
                switch(typeof appModel.config.selectedObject.hole)
                {
                    case "string":
                        hole = appModel.config.selectedObject.hole;
                        break;
                    case "object":
                        hole = appModel.config.selectedObject.hole[0];
                        break;
                }

                appModel.config.photosPerSession = appModel.config.selectedObject.photosPerSession || 1;
                trace("SHOWING THIS HOLE ",hole, "TOTAL PHOTOS PER SESSION:",appModel.config.photosPerSession)
                ICameraTexture(this._video).loadFrame( assetsModel.assets.getTexture(hole));
            }

            if(appModel.config.selectedObject && appModel.config.selectedObject.reference)
            {
                trace("REFERENCE")
                // hole es un string o un array? por compatibilidad con configuracioens anteriores que eran string
                var reference:String = "";
                switch(typeof appModel.config.selectedObject.reference)
                {
                    case "string":
                        reference = appModel.config.selectedObject.reference;
                        break;
                    case "object":
                        reference = appModel.config.selectedObject.reference[0];
                        break;
                }
                trace(reference)
                if(reference)
                {
                    Image(this._reference).visible = true;
                    Image(this._reference).source = assetsModel.assets.getTexture(reference);
                }
            }
        }

        override public function  initialize():void
        {
            super.initialize();

            Starling.current.skipUnchangedFrames = false;
            Starling.juggler.delayCall(this.dispatchWith,1,SoundEventType.PLAY_SOUND, true, "TakePictures");

            Button(this._nextButton).visible = false;

            // Eliminamos cualquier snapshot capturado de una session previa
            appModel.snapshot.clear();


        }



        override protected function movieClip_completeHandler(e:starling.events.Event):void
        {
            switch(MovieClip(e.currentTarget).name)
            {
                case "timer":
                    onTimerComplete();
                    break;
            }
        }

        private function onTimerComplete():void
        {
            this._timer.visible = false;

            if(appModel.config.hideFrameBeforeCapturePicture)
            {
                ICameraTexture(this._video).showFrame(false);
            }

            saveVideoFrame(ICameraTexture(this._video).getBitmapData())


        }


        protected function saveVideoFrame(bmd:BitmapData):void {


            if(appModel.config.hideFrameBeforeCapturePicture)
            {

                ICameraTexture(this._video).showFrame(true);
            }

            appModel.snapshot.addSnapshot(bmd);
            if(appModel.config.showPreviewAfterCapture)
            {
                tempByteArray.clear();
                bmd.encode(bmd.rect,new JPEGEncoderOptions(),tempByteArray)
                _previewTexture.push(Texture.fromBitmapData(bmd))
                var preview:Image = Image(screen.getChildByName("preview"+currentPhotoNumber));
                if(preview)
                {
                    preview.source =  _previewTexture[_previewTexture.length-1];
                    preview.visible = true;
                }

               if (ExternalInterface.available ) {
                   trace("Size: ",tempByteArray.length);
                   var path:String = ExternalInterface.call("saveTempImage", Base64.encode(tempByteArray),StringUtil.format("input{0}.jpg",currentPhotoNumber), "input");
                   appModel.inputFilesPath.push(path);
                   appModel.snapshot_path = path;
                   trace("20/03/2017 - Picture saved to: ",appModel.snapshot_path )
               }
               else
               {
                   // SOLO EN DEBUG
                   appModel.snapshot_path = "C:";
                   trace("Picture simulated to be saved to: ",appModel.snapshot_path )
               }

            }


            trace(currentPhotoNumber +"=="+ appModel.config.photosPerSession)


            // En casos de más de una toma, esperar un tiempo meintras mostramos el preview
            if(++currentPhotoNumber == appModel.config.photosPerSession)
            {
                // terminamos las tomas
                if(appModel.config.showPreviewAfterCapture)
                {
                    Starling.juggler.delayCall(showNextScreen,appModel.config.previewTimeBeforeCaptureNextPicture);
                }
                else
                {
                    this.showNextScreen();
                }
            }
            else
            {
                // aun no hemos terminado, habilitar nuevamente botón de capturar

                if(appModel.config.selectedObject && appModel.config.selectedObject.hole && appModel.config.selectedObject.hole[currentPhotoNumber])
                {
                    if(appModel.config.selectedObject.hole[currentPhotoNumber])
                    {
                        ICameraTexture(this._video).loadFrame( assetsModel.assets.getTexture(appModel.config.selectedObject.hole[currentPhotoNumber]));
                    }
                    else
                    {
                        trace("ERROR: no hay hole configurado para la toma: ",currentPhotoNumber);
                    }

                }

                // previsualizar la referencia de la foto, solo cuando sean 2 o más
                if(appModel.config.selectedObject && appModel.config.selectedObject.reference && appModel.config.selectedObject.reference[currentPhotoNumber])
                {
                    if(appModel.config.selectedObject.reference[currentPhotoNumber])
                    {
                        Image(this._reference).source = assetsModel.assets.getTexture(appModel.config.selectedObject.reference[currentPhotoNumber]);
                    }
                }


                this._captureButton.visible = true;
                Button(this._captureButton).isEnabled = true;
            }
        }

        private function showNextScreen():void
        {
            this._video.removeFromParent(true);


            // No se crearon texturas ni se codificaron bmd, tenemos que hacerlo aqui
            if(!appModel.config.showPreviewAfterCapture)
            {
                trace("WE NEED TO SAVE THE PICTURES HERE")
                for(var i:uint = 0, l:uint = appModel.snapshot.length; i < l; i++)
                {
                    trace("STORING ",i, "l ",l);
                    tempByteArray.clear();
                    var bmd:BitmapData = appModel.snapshot.getSnapshot(i);
                    bmd.encode(bmd.rect,new JPEGEncoderOptions(),tempByteArray);

                    _previewTexture.push(Texture.fromBitmapData(bmd))

                    if (ExternalInterface.available ) {
                        trace("Size: ",tempByteArray.length);
                        var path:String = ExternalInterface.call("saveTempImage", Base64.encode(tempByteArray),StringUtil.format("input{0}.jpg",i), "input");
                        appModel.inputFilesPath.push(path);
                        appModel.snapshot_path = path;
                        trace("20/03/2017 - Picture saved to: ",appModel.snapshot_path )
                    }
                    else
                    {
                        // SOLO EN DEBUG
                        appModel.snapshot_path = "C:";
                        trace("Picture simulated to be saved to: ",appModel.snapshot_path )
                    }
                }
            }




            // decidir si permitimos o no seleccionar punto de color
            if(appModel.config.chooseColorFromLastPreview)
            {


                Starling.current.skipUnchangedFrames = true;
                Image(this._preview).source = _previewTexture[_previewTexture.length-1];
                Image(this._preview).visible = true;
                if(this._pixelTitle)
                    Image(this._pixelTitle).visible = true;

                this._captureButton.visible = false;
                Button(this._nextButton).visible = false;
                PixelViewer(this._pixelViewer).texture = _previewTexture[_previewTexture.length-1];


            }
            else
            {
                this.dispatchWith(AppEventType.APP_NEXT_SCREEN,true,{screen:screen});
            }

        }


        override protected function image_touchHandler(event:TouchEvent):void
        {
            super.image_touchHandler(event);

            switch(DisplayObject(event.target).name)
            {
                case "preview":
                    var touches = event.getTouches(DisplayObject(event.target));

                    if(touches.length == 1 && (touches[0].phase == TouchPhase.MOVED || touches[0].phase == TouchPhase.BEGAN)) {


                        this._pixelViewer.visible = true;
                        Button(this._nextButton).visible = true;
                        var point:Point = touches[0].getLocation(Image(event.target).internalImage, Pool.getPoint())
                        PixelViewer(this._pixelViewer).showPoint(point);
                        Pool.putPoint(point);
                    }
                    break;
            }



        }

        override protected function button_triggeredHandler(e:Event):void
		{
			super.button_triggeredHandler(e);
			
			switch(Button(e.currentTarget).name)
			{
				case "capture-button":
                    captureNextPicture();

					break;
                case "next-button":
                    if(PixelViewer(this._pixelViewer).pixelSelected)
                    {
                        var bmd:BitmapData = PixelViewer(this._pixelViewer).getPixelColor();

                        if (ExternalInterface.available) {
                            tempByteArray.clear()
                            bmd.encode(bmd.rect,new JPEGEncoderOptions(),tempByteArray);
                            var path:String = ExternalInterface.call("saveTempImage", Base64.encode(tempByteArray),StringUtil.format("color.jpg",currentPhotoNumber), "input");
                            appModel.inputFilesPath.push(path);
                            trace("Color saved to: ",path );


                        }
                        else
                        {
                            // SOLO EN DEBUG
                            trace("Color simulated to be saved ", bmd.rect);
                        }
                        bmd.dispose();
                    }

                    // Guardar la imagen de color, si han elegido
                    this.dispatchWith(AppEventType.APP_NEXT_SCREEN,true,{screen:screen});
                    break;
			}
		}

        private function captureNextPicture():void
        {
            if(!Starling.juggler.contains(this._timer))
            {
                Starling.juggler.add(this._timer);
            }

            // Debemos mostrar el timer siempre en la primer toma, en Faceme siempre mostrarlo
            this._timer.visible = true;
            this._timer.stop();
            this._timer.play();
            Button(this._captureButton).isEnabled = false;
        }

		override public function onRemove():void
		{
            if(_previewTexture)
            {
                _previewTexture.forEach(function(item:Texture, index:int, vector:Vector.<Texture>):void{
                    item.dispose();
                })
                _previewTexture = null;
            }

            while(screen.numChildren>0)
            {
                screen.getChildAt(0).removeFromParent(true);
            }

            if(Starling.juggler.contains(this._timer))
            {
                Starling.juggler.remove(this._timer);
            }

			trace("onRemove::StartScreenMediator")
			super.onRemove();
		}

	}
}