package com.dedosmedia.views.mediators
{
import by.blooddy.crypto.Base64;

import com.dedosmedia.component.TouchSheet;
import com.dedosmedia.component.manager.TouchSheetManager;
import com.dedosmedia.events.AppEventType;
import com.dedosmedia.events.ErrorEventType;
import com.dedosmedia.events.NativeProcessEventType;
import com.dedosmedia.events.SoundEventType;
import com.dedosmedia.model.AppModel;

import flash.display.Bitmap;
import flash.display.BitmapData;

import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.display.PNGEncoderOptions;

import flash.geom.Point;
import flash.geom.Rectangle;
import flash.geom.Rectangle;
import flash.net.URLRequest;

import starling.display.BlendMode;

import starling.display.DisplayObject;

import flash.display.JPEGEncoderOptions;


import com.dedosmedia.component.Button;
import feathers.controls.ImageLoader;

import flash.display.BitmapData;
import flash.external.ExternalInterface;
import flash.media.SoundChannel;
import flash.utils.ByteArray;

import com.dedosmedia.component.Image;

import starling.core.Starling;
import starling.display.DisplayObjectContainer;
import starling.display.MovieClip;
import starling.display.MovieClip;
import starling.filters.ColorMatrixFilter;
import starling.filters.FragmentFilter;
import starling.textures.RenderTexture;
import starling.textures.SubTexture;
import starling.utils.Color;

//import starling.display.Image;
import starling.display.MovieClip;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.textures.Texture;
import starling.utils.Pool;
import starling.utils.RectangleUtil;
import starling.utils.ScaleMode;
import starling.utils.StringUtil;

public class FilterScreenMediator extends CustomMediator
{

        private var bitmapData:BitmapData;
        private var _workingChannel:SoundChannel;


        private var _touchManager:TouchSheetManager;

        private var _working:DisplayObjectContainer;
        private var _highLightCurrentFilter:DisplayObject;
        private var _original:DisplayObject;
        private var _preview:DisplayObject;
        private var _gif:DisplayObject;
        private var _background:DisplayObject;
        private var _overlay:DisplayObject;
        private var _automaticFilterButton:DisplayObject;
        private var _automaticBackgroundButton:DisplayObject;
        private var _automaticOverlayButton:DisplayObject;
        private var _logo:DisplayObject;
        private var _smoke:DisplayObject;
        private var _continueButton:DisplayObject;
        private var _takeAgainButton:DisplayObject;


        private var _preview_bmd:BitmapData;
        private var _preview_bmd_arr:Vector.<BitmapData> = new Vector.<BitmapData>();
        private var _preview_texture:Texture;
        private var _preview_texture_arr:Vector.<Texture> = new Vector.<Texture>();


        private var _holder:Sprite;         // Contiene tdos los elementos de la foto, background, imagen, props


        private var currentGIFFrame:uint = 0;
        private var frameLoader:Loader = new Loader();          // Se encarga de cargar de disco las imagenes output procesadas en los GIF
        private var frameByteArray:Vector.<ByteArray> = new Vector.<ByteArray>();


        private var logoLoader:Loader = new Loader();          // Se encarga de cargar de disco la imagen del logo
        private var logoBitmapData:BitmapData;

        private var preview_sheet:TouchSheet;       // TouchSheet cuando el picture es Draggable
        private var preview_sheet_arr:Vector.<TouchSheet>;
        private var current_preview_sheet:int = 0;



        private var _customPivot:Point = new Point(360,360);


        // Los path a los archivos de la imagen de salida, que se deben enviar a S3. Cuando sendPictureFromAE no está activado, se deben guardar nuevamente los ficheros
        private var framesFilePath:Array = new Array();
        private var sessionCode:String;
        //private var locationCode:String;


        private var _hasHandWrite:Boolean = false;
        private var _renderTexture:RenderTexture;
        private var _canvas:starling.display.Image;
        private var _brush:starling.display.Image;
        private var _color:uint = 0xFF0000;


		override public function onRegister():void
		{
			trace("onRegister FilterScreenMediator ");

			super.onRegister();

            this._holder = new Sprite();
            this._original  = getChild("original") as DisplayObject;
            this._preview   = getChild("preview0") as DisplayObject;
            this._gif   = getChild("gif") as DisplayObject;
            this._working   = getChild("working") as DisplayObjectContainer;
            this._smoke   = getChild("smoke") as DisplayObject;
            this._logo   = getChild("logo") as DisplayObject;
            this._background   = getChild("greenscreen-background") as DisplayObject;
            this._overlay   = getChild("overlay") as DisplayObject;
            this._continueButton   = getChild("continue-button") as DisplayObject;
            this._takeAgainButton   = getChild("take-again-button") as DisplayObject;

            this._highLightCurrentFilter = getChild(appModel.config.highLightCurrentFilter);
            this._automaticFilterButton = getChild(appModel.config.automaticFilter) as DisplayObject;
            this._automaticBackgroundButton = getChild(appModel.config.automaticBackground) as DisplayObject;
            this._automaticOverlayButton = getChild(appModel.config.automaticOverlay) as DisplayObject;

            this._touchManager = new TouchSheetManager(Starling.current.stage);
            this._touchManager.addEventListener(TouchSheetManager.DROPPED, onDrop);
            this._touchManager.addEventListener(TouchSheetManager.DRAGGED, onDrag);
            this._touchManager.addEventListener(TouchSheetManager.DEPTH_CHANGED, onDepthChanged);

            preview_sheet_arr = new Vector.<TouchSheet>(appModel.config.photosPerSession);

		}

        override  public function initialize():void
        {
            super.initialize();


            sessionCode = ExternalInterface.call("getVariable", "GUI.CurrentSessionCode");
            _hasHandWrite = ExternalInterface.call("getVariable", "hasHandWrite");

            if(_hasHandWrite && _renderTexture == null)
            {

                loadLogo();

                _renderTexture = new RenderTexture(_original.width, _original.height);
                this._canvas = new starling.display.Image(this._renderTexture);
                screen.addChild(_canvas);
                this._canvas.x = _original.x;
                this._canvas.y = _original.y;
                this._canvas.addEventListener(TouchEvent.TOUCH, onTouchCanvas);

                _brush = new starling.display.Image(assetsModel.assets.getTexture("brush"));
                _brush.pivotX = _brush.width / 2;
                _brush.pivotY = _brush.height / 2;
                _brush.blendMode = BlendMode.NORMAL;


                var handWriteColor = ExternalInterface.call("getVariable", "handWriteColor")
                _color = handWriteColor || _color;
            }



            Starling.current.skipUnchangedFrames = true;
            this.addContextListener(NativeProcessEventType.NATIVEPROCESS_COMPLETED,nativeProcessCompleted_handler,Event);


            // Pueda ser  q esto  bloquee input? y por eso renderice un viejo?

            if(_original)
            {
                Image(_original).source = "temp/input/input0.jpg";
            }

            //ImageLoader(this._preview).source = "temp/input0.jpg";
            //Image(this._preview).saveBitmapData = true;
            //Image(this._preview).source = "temp/output0.png";

            appModel.localConnection.addEventListener(AppEventType.APP_ERROR_LOCAL_CONNECTION,onLocalConnectionError);


            if(_automaticFilterButton)
            {
                _automaticFilterButton.dispatchEventWith(Event.TRIGGERED);
            }

            if(_automaticBackgroundButton)
            {
                _automaticBackgroundButton.dispatchEventWith(Event.TRIGGERED);
            }

            if(_automaticOverlayButton)
            {
                _automaticOverlayButton.dispatchEventWith(Event.TRIGGERED);
            }

            if(appModel.config.selectedObject)
            {
                if(appModel.config.selectedObject.overlay)
                {
                    ImageLoader(_overlay).source = assetsModel.assets.getTexture(appModel.config.selectedObject.overlay);
                }


                if(appModel.config.selectedObject.filter)
                {
                    this._automaticFilterButton = getChild(appModel.config.selectedObject.filter) as DisplayObject;
                    _automaticFilterButton.dispatchEventWith(Event.TRIGGERED);
                    highLightButton(_automaticFilterButton);
                }

            }
            

            this._preview.addEventListener(TouchEvent.TOUCH,onPreviewTouch);

        }

        private function onTouchCanvas(event:TouchEvent):void
        {
            // touching the canvas will draw a brush texture. The 'drawBundled' method is not
            // strictly necessary, but it's faster when you are drawing with several fingers
            // simultaneously.

            this._renderTexture.drawBundled(function():void
            {
                var touches:Vector.<Touch> = event.getTouches(_canvas);

                for each (var touch:Touch in touches)
                {

                    if (touch.phase == TouchPhase.HOVER || touch.phase == TouchPhase.ENDED)
                        continue;

                    var location:Point = touch.getLocation(_canvas,Pool.getPoint());
                    var lastLocation:Point = touch.getPreviousLocation(_canvas,Pool.getPoint());

                    _brush.x = location.x;
                    _brush.y = location.y;
                    _brush.color = _color;
                    //_brush.rotation = Math.random() * Math.PI * 2.0;

                    // pintar solo los puntos en las ubicaciones que llegan los TouchEvent
                    _renderTexture.draw(_brush);

                    // completar los espacios entre las uicaciones de los touchEvent
                    if(lastLocation)
                    {
                        var dist:Number = Math.sqrt(Math.pow(location.x - lastLocation.x, 2) +  Math.pow(location.y - lastLocation.y, 2));
                        var steps:int = 20 //dist/5;
                        for (var i:int=0; i<steps; ++i)
                        {
                            var drawPos:Point = Point.interpolate(location, lastLocation, i/steps);
                            _brush.x = drawPos.x;
                            _brush.y = drawPos.y;
                            //_brush.rotation = Math.random() * Math.PI * 2.0;
                            _renderTexture.draw(_brush);
                        }
                    }

                    // necessary because 'Starling.skipUnchangedFrames == true'
                    screen.setRequiresRedraw();
                }

                Pool.putPoint(location);
                Pool.putPoint(lastLocation);
            });

            bringOverlayToFront();
        }


        private function onPreviewTouch(event:TouchEvent):void {

            var touches:Vector.<Touch> = event.getTouches(event.target as DisplayObject,null);
            if(touches.length == 1) {
                switch (touches[0].phase)
                {
                    case TouchPhase.BEGAN:
                        if(this._original)
                        {
                            this._original.visible = true;
                            this._preview.visible = !this._original.visible;
                        }
                        break;
                    case TouchPhase.ENDED:
                        if(this._original)
                        {
                            this._original.visible = false;
                            this._preview.visible = !this._original.visible;
                        }
                        break;
                }
            }
        }


        private function nativeProcessCompleted_handler(event:Event):void
        {

            // Determinar si nos está mandando uno o varias imágenes, y si estamos en modo GIF o tirillas

            if(appModel.config.isGIF && this._gif)
            {

                for(var i:int = 0, l:int=appModel.config.photosPerSession; i<l; i++)
                {
                    trace("add ",i)
                    assetsModel.assets.enqueue(StringUtil.format("temp/output{0}.{1}",i,appModel.config.outputModule));
                }
                trace("load")
                assetsModel.assets.loadQueue(onFinishLoadOutput);
            }
            else
            {

                // Cuando no sea GIF, también es posible que haya más de una foto por sesión... por ej: Faceme Multiple

                for(var i:int = 0, l:int = appModel.config.photosPerSession; i < l; i++)
                {
                    // BREAKING CHANGE: ya no debe llamarse preview, si no preview0
                    this._preview   = getChild("preview"+i) as DisplayObject;

                    // imagen normal en el preview
                    Image(this._preview).source = null;
                    Image(this._preview).saveBitmapData = true;
                    var source:String = StringUtil.format("temp/output{0}.{1}",i,appModel.config.outputModule);
                    ImageLoader(this._preview).source = source;
                }
                stopWorking();
            }


        }

    private function onFinishLoadOutput(progress:Number)
    {
        if(progress <1)
        {
            return;
        }

        trace("load finished");
        var clip:MovieClip = MovieClip(this._gif);
        while(clip.numFrames > 1){
            clip.removeFrameAt(0);
        }

        trace(appModel.config.selectedObject)
        if(appModel.config.selectedObject)
        {
            trace(appModel.config.selectedObject.gifSequence)
        }

        var gifBoomerang:Boolean = ExternalInterface.call("getVariable", "gifBoomerang") || false;
        var frameOrder:Array = createOrderArray(appModel.config.photosPerSession, gifBoomerang);
        trace("GIF BOOMERANG: ",gifBoomerang," FRAMEORDER: ",frameOrder," GIFDURATION ",appModel.config.gifDuration);

        for (var i:int=0, l:int=frameOrder.length; i<l; i++)
        {
            clip.addFrame(assetsModel.assets.getTexture( StringUtil.format("output{0}",frameOrder[i])),null, appModel.config.gifDuration/1000);// 1/clip.fps); //0.2);
        }
        // remove that last previous frame
        clip.removeFrameAt(0);

        // set to frame 1
        clip.currentFrame = 1;

        //Reajust the clip size
        clip.readjustSize();

        if(!Starling.juggler.contains(MovieClip(this._gif)))
        {
            Starling.juggler.add(MovieClip(this._gif));
        }

        this._gif.visible = true;


        // Redimnesionar al tamaño del preview, cuando el GIF sale al tamaño del AE.
        var _outRectangle:Rectangle = RectangleUtil.fit(_gif.bounds,this._preview.bounds,ScaleMode.SHOW_ALL,false,Pool.getRectangle());
        _gif.width = _outRectangle.width;
        _gif.height = _outRectangle.height;
        _gif.x = _outRectangle.x;
        _gif.y = _outRectangle.y;

        MovieClip(this._gif).stop();
        MovieClip(this._gif).play();
        stopWorking();
    }

    /*
     * Genera un array con una secuencia consecutiva de numeros o numeros en boomerang
     */
    private function createOrderArray(length:uint = 0, isBoomerang:Boolean = false):Array
    {
        var i:uint = 0;
        var temp:Array = new Array();
        while(i < length)
        {
            temp.push(i);
            i++;
        }

        if(isBoomerang)
        {
            var reverse:Array = temp.slice();
            reverse.pop();
            reverse.shift();
            reverse.reverse();
            temp = temp.concat(reverse);
        }

        return temp;
    }

    private function trimAlpha(source:BitmapData, required:Boolean = false):BitmapData {

        trace("trim ",appModel.config.trimAlphaEnabled);

            if(!appModel.config.trimAlphaEnabled && !required)
            {
                return source;
            }

            var notAlphaBounds:Rectangle = source.getColorBoundsRect(0xFF000000, 0x00000000, false);
            var trimed:BitmapData = new BitmapData(notAlphaBounds.width, notAlphaBounds.height, true, 0x00000000);
            trimed.copyPixels(source, notAlphaBounds, new Point());
            return trimed;
        }

        override protected function image_completeHandler(event:Event):void
        {
            super.image_completeHandler(event);

            var _name:String = Image(event.currentTarget).name.indexOf("preview")!=-1?"preview":"";
            var _index:int = Image(event.currentTarget).name.split("preview")[1];
            trace("_name: ",_name,"_index: ",_index);
            switch (_name)
            {
                case "preview":
                    trace("isPictureDraggable ",appModel.config.isPictureDraggable);
                    if(appModel.config.isPictureDraggable)
                    {
                        trace("is enabled? ",Image(event.currentTarget).saveBitmapData);
                        //var pictureRect:Rectangle = Image(event.currentTarget).bmd.getColorBoundsRect(0xFF000000, 0x00000000, false);

                        // Si la imagen es draggable intenta recortar los bordes que sean transparentes
                        this._preview_bmd = trimAlpha(Image(event.currentTarget).bmd)
                        _preview_bmd_arr.push(_preview_bmd);
                        this._preview_texture = Texture.fromBitmapData(this._preview_bmd, appModel.config.antialiasingEnabled)
                        _preview_texture_arr.push(_preview_texture);


                        // presionar el botón de face0 por defecto
                        var face:Button = Button(screen.getChildByName("face"+_index));
                        if(face && _index == 0)
                        {
                            face.dispatchEventWith(Event.TRIGGERED);
                        }

                        // Visualizar miniaturas de face, solo si hay más de 2 rostros en un faceme
                        if(appModel.config.photosPerSession > 1)
                        {
                            if(face)
                            {
                                face.visible = true;
                                _highLightCurrentFilter.alpha = 1;
                            }
                        }




                        var tr:Texture = assetsModel.assets.getTexture("handleTr");
                        var bl:Texture = assetsModel.assets.getTexture("handleBl");
                        var border:Texture = assetsModel.assets.getTexture("border");
                        var iconEnabled:Boolean = appModel.config.isFaceme != true;
                        preview_sheet = new TouchSheet(this._preview_texture, tr, bl, border, iconEnabled);
                        preview_sheet.name = "ps"+_index;
                        preview_sheet_arr[_index] = preview_sheet;
                        if(appModel.config.isFaceme)
                        {
                            // enmascararmos el touchsheet, dejamos preview visible para que no pierda eventos
                            preview_sheet.mask = this._preview;
                            preview_sheet.bringToFront = false;
                        }
                        else
                        {
                            // si es paparazzi movible, debemos hacer inivisible la imagen, no habrá mascara
                            preview_sheet.bringToFront = true;
                            Image(this._preview).visible = false;
                        }

                        preview_sheet.isRemovable = false;
                        screen.addChild(preview_sheet);
                        this._touchManager.add(preview_sheet);
                        // Ajustar la imagen del preview, al máximo tamaño indicado
                        var _outRectangle:Rectangle = RectangleUtil.fit(preview_sheet.bounds,this._preview.bounds,ScaleMode.SHOW_ALL,false,Pool.getRectangle());
                        preview_sheet.setSize(_outRectangle.width, _outRectangle.height);
                        preview_sheet.x = this._preview.x + this._preview.width/2;
                        preview_sheet.y = this._preview.y + this._preview.height/2;
                        trace("PivotX ",preview_sheet.pivotX, preview_sheet.pivotY);

                        Pool.putRectangle(_outRectangle);

                        this.bringOverlayToFront();

                    }

                    break;
            }


        }

        private function stopWorking():void
        {
            if(this._working)
            {
                this._working.visible = false;

                if(this._working.getChildByName("processing"))
                {
                    Starling.juggler.remove(MovieClip(this._working.getChildByName("processing")));
                }

                if(this._workingChannel)
                    this._workingChannel.stop();
            }

        }


        private var writeOn:Function = function(settings:*):void
        {

            switch (typeof settings)
            {
                case "string":
                    trace("ERROR SETTINGS MAL CONFIGURADOS, DEBEN SER UN OBJECT")
                    break;
                case "object":

                    var value:Number = settings.value;
                    switch(settings.property) {
                        case "erase":
                            trace("CLEAR");
                            _renderTexture.clear();
                            screen.setRequiresRedraw();
                            break;
                    }
            }
        }


        private var changeProperty:Function = function(settings:*):void
        {
            if(settings.property == "current") {
                current_preview_sheet = Number(settings.value);
                highLightButton(this);
            }
            preview_sheet = preview_sheet_arr[current_preview_sheet];

            // Ponerle foco para activar gestos en  este elemento
            preview_sheet.bringToFront = true;
            _touchManager.setFocus(preview_sheet);
            preview_sheet.bringToFront = false;
            bringOverlayToFront();




            if(appModel.config.selectedObject && appModel.config.selectedObject.pivot)
            {
                _customPivot.x = appModel.config.selectedObject.pivot[current_preview_sheet].x;
                _customPivot.y = appModel.config.selectedObject.pivot[current_preview_sheet].y;
            }

            // Si no es ColorMatrixFilter, seguramente es otro tipo de FragmentFilter, reemplazarlo
            if(!(preview_sheet.filter is ColorMatrixFilter))
            {
                preview_sheet.filter = new ColorMatrixFilter();
            }
            var filter:ColorMatrixFilter = preview_sheet.filter as ColorMatrixFilter;

            switch (typeof settings)
            {
                case "string":
                    trace("ERROR SETTINGS MAL CONFIGURADOS, DEBEN SER UN OBJECT")
                    break;
                case "object":

                        var value:Number = settings.value;

                        switch(settings.property)
                        {
                            case "contrast":
                                filter.adjustContrast(value);
                                break;
                            case "brightness":
                                filter.adjustBrightness(value);
                                break;
                            case "saturation":
                                filter.adjustSaturation(value);
                                break;
                            case "hue":
                                filter.adjustHue(value);
                                break;
                            case "rotation":
                                movePivotToPoint(_customPivot);
                                preview_sheet.rotation += value;
                                break;
                            case "scale":
                                movePivotToPoint(_customPivot);
                                preview_sheet.scale += value;
                                break;
                            case "reset":
                                preview_sheet.pivotX = 0;
                                preview_sheet.pivotY = 0;
                                preview_sheet.scale = 1;
                                preview_sheet.rotation = 0;
                                preview_sheet.x = _preview.x + _preview.width/2;
                                preview_sheet.y = _preview.y + _preview.height/2;
                                filter.reset();
                                break;
                            case "position_x":
                                preview_sheet.x += value;
                                break;
                            case "position_y":
                                preview_sheet.y += value;
                                break;
                        }
                     break;
            }

        }

    private function movePivotToPoint(pivot:Point):void
    {
        var localPivot:Point = new Point();
        var globalPivot:Point = new Point();
        _preview.localToGlobal(pivot,globalPivot);
        preview_sheet.globalToLocal(globalPivot,localPivot)
        var _lastX = preview_sheet.bounds.x;
        var _lastY = preview_sheet.bounds.y;
        preview_sheet.pivotX =  localPivot.x;
        preview_sheet.pivotY =  localPivot.y;
        var _newX = preview_sheet.bounds.x;
        var _newY = preview_sheet.bounds.y;
        preview_sheet.x += _lastX-_newX;
        preview_sheet.y += _lastY-_newY;
    }


        // El scope de esta función es el botón presionado, así que this se refiere al botón
        private var applyBackground:Function = function(settings:*):void//function(texture:String):void
        {


            trace("typeof ",typeof settings)
            var texture:String = "";
           switch (typeof settings)
           {
               case "string":
                       texture = settings;
                   break;
               case "object":

                       /*
                       "callback": {
                       "function": "applyBackground",
                         "args": [
                                   {
                                       "background": "dynamic-greenscreen{0}",
                                       "isDynamic": true,
                                       "placeholder": ["selectedBackground"]
                                   }
                               ]
                           }
                        */
                       // Toma variables de appModel.config, indicadas en el array de placeholder, y las reemplazada en el string de background {0},{1}...

                       var args:Array = [settings.background];
                       trace("settings placeholder",settings.placeholder)

                       if(settings.placeholder)
                       {
                           for(var i in settings.placeholder)
                           {
                               var temp = appModel.config[settings.placeholder[i]]
                               args.push(temp)
                           }
                       }

                       texture = StringUtil.format.apply(null, args)

                   break;
           }

            trace("applYbackgropund ",this, texture);

            if(_background)
            {
                ImageLoader(_background).source = assetsModel.assets.getTexture(texture);
            }
            else
            {
                throw new Error("No has configurado ningun elemento greenscreen-background donde cargar el fondo")
            }

            trace("higlight");
            highLightButton(this);
        }


    // El scope de esta función es el botón presionado, así que this se refiere al botón
    private var applyBackgroundAndOverlay:Function = function(settings:*):void//function(texture:String):void
    {


        trace("typeof ",typeof settings)
        var background:String = "";
        var overlay:String = "";
        switch (typeof settings)
        {
            case "string":
                   trace("ERROR no configurado correctamente")
                break;
            case "object":
                background = settings.background;
                overlay = settings.overlay;
                break;
        }

        trace("applyBackground ", background);
        trace("applyOverlay", overlay);


        if(_background)
        {
            ImageLoader(_background).source = assetsModel.assets.getTexture(background);
        }
        else
        {
            throw new Error("No has configurado ningun elemento greenscreen-background donde cargar el fondo")
        }


        if(_logo)
        {
            ImageLoader(_logo).source = assetsModel.assets.getTexture(overlay);
        }
        else
        {
            throw new Error("No has configurado ningun elemento greenscreen-overlay donde cargar el overlay")
        }

        trace("higlight");
        highLightButton(this);
    }

    // El scope de esta función es el botón presionado, así que this se refiere al botón
    private var applyOverlay:Function = function(texture:String):void
    {
        trace("applyOverlay ",this, texture);
        ImageLoader(_overlay).source = assetsModel.assets.getTexture(texture);
        highLightButton(this);
    }

        // El scope de esta función es el botón presionado, así que this se refiere al botón
        //private var applyFilter:Function = function(plugin:String = null, composition:String = null, outputModule:String = "jpg", frameSettings:Object = null):void
        private var applyFilter:Function = function(settings:Object = null):void
        {


            var start:Number = 0;
            var end:Number = 0;
            var increment:Number = 1;
            var composition:String = "master";
            var file:String = null;
            var outputModule:String = "jpg";

            if(settings)
            {
                start = settings.start?settings.start:start;
                end = settings.end?settings.end:end;
                increment = settings.increment?settings.increment:increment;
                composition = settings.composition?settings.composition:composition;
                file = settings.file?settings.file:file;
                outputModule = settings.outputModule?settings.outputModule:outputModule;
            }

            trace(this, " APPLY FILTER composition:",composition,"input: ",appModel.snapshot_path," file: ",file, "outputModule ",outputModule,"start:",start,"end:",end,"increment:",increment);

            if(_working)
            {
                _working.visible = true;
                _working.parent.addChild(_working);
                _workingChannel = assetsModel.assets.playSound("working",0,99);
                if(_working.getChildByName("processing")) {
                    Starling.juggler.add(MovieClip(_working.getChildByName("processing")));
                }
            }

            highLightButton(this);






            appModel.config.outputModule = outputModule;
            var dstPath:String = "temp/input/input0.jpg";   // ya no se va a seguir usando, se mantiene por compatibilidad
            appModel.localConnection.send("app#com.dedosmedia.Keshot","echo",{"message":"AIR invocado desde Flash. OK","plugin":file, "composition":composition, "input":appModel.snapshot_path, "om":outputModule, "dstPath":dstPath, "projectPath":appModel.url});
            appModel.localConnection.send("app#com.dedosmedia.Keshot","receiveMessage",
                    {
                        "aepx":file,
                        "composition":composition,
                        "input":appModel.snapshot_path,
                        "outputModule":outputModule,
                        "dstPath":dstPath,    // ya no se va a seguir usando, se mantiene por compatibilidad
                        "projectPath":appModel.url,
                        "start": start,
                        "end": end,
                        "increment": increment
                    })

        };

        private function highLightButton(button):void
        {

            trace("higlight function ",button, this, "--- current: ",_highLightCurrentFilter);
            // Poner marco alrededor del filtro seleccionado, solo si el marco existe
            if(_highLightCurrentFilter)
            {
                screen.setChildIndex(_highLightCurrentFilter,screen.numChildren-1);
                _highLightCurrentFilter.visible = true;


                var _highLighBounds:Rectangle = _highLightCurrentFilter.bounds;
                var _outRectangle:Rectangle = RectangleUtil.fit(_highLighBounds,Button(button).bounds,ScaleMode.NONE,false,Pool.getRectangle());
                _highLightCurrentFilter.x = _outRectangle.x;
                _highLightCurrentFilter.y = _outRectangle.y;

                trace(Button(button).name, Button(button).bounds);
                Pool.putRectangle(_outRectangle);

            }
        }

        private function onLocalConnectionError(event:Event):void {
            trace("ERROR EN LOCAL CONNECTIOn");
            //stopWorking();
            //ImageLoader(this._preview).source = StringUtil.format("temp/output0.{0}",appModel.config.outputModule);
            this.dispatchWith(ErrorEventType.ERROR,true,{text:"Error en llamada a LocalConnection. Verifica que DedosMeia-Keshot está siendo ejecutada", error:"LocalConnection"})


            // imagen normal en el preview
            Image(this._preview).source = null;
            Image(this._preview).saveBitmapData = true;
            var source:String = StringUtil.format("temp/output0.{0}",appModel.config.outputModule);
            ImageLoader(this._preview).source = source;
            stopWorking();




        }

        //TODO: Podria hacerse que el componente mismo escuche por el evento y si tiene propiedad autoHide, hacer que se esconda cuando se complete
        override protected function movieClip_completeHandler(e:Event):void {

            super.movieClip_completeHandler(e);

            switch(MovieClip(e.currentTarget).name)
            {
                case "smoke":
                    MovieClip(e.currentTarget).visible = false;
                    MovieClip(e.currentTarget).stop();
                    break;
            }
        }

        override protected function draggableImage_droppedHandler(e:Event):void {
            super.draggableImage_droppedHandler(e);

            trace("TOUCH EN ",e.data.touch);

            //var globalPoint:Point = new Point(e.data.touch.globalX, e.data.touch.globalY);
            //var dropTarget:DisplayObject = Starling.current.stage.hitTest(globalPoint);

            var bounds1:Rectangle = DisplayObject(e.currentTarget).bounds;
            var bounds2:Rectangle = this._preview.bounds;

            if (bounds1.intersects(bounds2))
            {
                var sheet:TouchSheet = super.createItemFromJSON(e.data.json,screen) as TouchSheet;
                sheet.transformationMatrix = DisplayObject(e.currentTarget).transformationMatrix;
                sheet.x += DisplayObject(e.currentTarget).pivotX;
                sheet.y += DisplayObject(e.currentTarget).pivotY;
                sheet.setSize(DisplayObject(e.currentTarget).width, DisplayObject(e.currentTarget).height);
                screen.addChild(sheet);
                this._touchManager.add(sheet);
            }

            this.bringOverlayToFront();
        }


        private function onDepthChanged():void
        {
            trace("Depth Changed")
            // Bring Logo to front


           //this.bringOverlayToFront();
        }

        private function bringOverlayToFront()
        {
            if(this._overlay)
            {
                this._overlay.parent.addChild(this._overlay);
            }

            if(this._logo)
            {
                this._logo.parent.addChild(this._logo);
            }
        }

        // Mandar al frente al Drag mientras se arrastra
        private function onDrag(event:*):void {
            trace("onDrag")
        }
        // Remover un TouchSheet cuando se suelta fuera del preview
        private function onDrop(e:Event):void {

            var globalPoint:Point = new Point(e.data.touch.globalX, e.data.touch.globalY);
            var bounds1:Rectangle = TouchSheet(e.data.target).bounds;
            var bounds2:Rectangle = this._preview.bounds;

            if (!bounds1.intersects(bounds2))
            {

              if(!TouchSheet(e.data.target).isRemovable)
                  return;

                // eliminar objeto
                this.dispatchWith(SoundEventType.PLAY_SOUND,true,"explosion");

                if(this._smoke)
                {
                    screen.addChild(this._smoke);
                    this._smoke.alignPivot();
                    this._smoke.x = globalPoint.x;
                    this._smoke.y = globalPoint.y;
                    this._smoke.visible = true;
                    Starling.juggler.add(MovieClip(this._smoke));
                    MovieClip(this._smoke).play();
                }


                this._touchManager.remove(TouchSheet(e.data.target));
                TouchSheet(e.data.target).destroy();
            }

            this.bringOverlayToFront();

        }

        override protected function button_triggeredHandler(e:Event):void
		{
			super.button_triggeredHandler(e);

            trace("ARGS ",Button(e.currentTarget).args);

            if(Button(e.currentTarget).callback != "")
            {
                try
                {
                    trace("buuton: ",Button(e.currentTarget).callback)
                    trace(e.currentTarget)
                    trace(Button(e.currentTarget).name)
                    trace(this[Button(e.currentTarget).callback]);
                    this[Button(e.currentTarget).callback].apply(Button(e.currentTarget),Button(e.currentTarget).args);
                }
                catch(e:Error)
                {
                    trace("ERROR: Button  callback. ",e.getStackTrace());
                }
            }

			switch(Button(e.currentTarget).name)
			{
				case "take-again-button":

                        //screen.dispatchEventWith(Event.CLOSE);
                        this.dispatchWith(AppEventType.APP_BACK_SCREEN,true,{screen:screen});
					break;
                case "continue-button":
                        trace("SEND PICTURE TO GUI");
                        _touchManager.killFocus();




                        appModel.localConnection.close();

                        // En GIF siempre se envía lo que salga de AE... no se captura snapshot
                        if(appModel.config.isGIF)
                        {
                            trace("isGIF")
                            if (ExternalInterface.available) {
                                if(this._working)
                                    this._working.visible = true;

                                Button(_continueButton).isEnabled = false;
                                Button(_takeAgainButton).isEnabled = false;


                                currentGIFFrame = 0;
                                framesFilePath.length = 0;

                                loadFrame();

                            }
                        }
                        else
                        {
                            // Captura la imagen desde la pantalla
                            var ba :ByteArray = new ByteArray();
                            trace("sendPicturefromAE ",appModel.config.sendPictureFromAE)

                            if( bitmapData)
                            {
                                bitmapData.dispose();
                                bitmapData = null;
                            }

                            if(appModel.config.sendPictureFromAE)
                            {
                                bitmapData = Image(this._preview).bmd;
                                bitmapData.encode(bitmapData.rect,new JPEGEncoderOptions(100),ba);
                            }
                            else
                            {

                                var stageBmd:BitmapData = Starling.current.stage.drawToBitmapData();
                                var bounds:Rectangle = this._preview.bounds;
                                bitmapData = new BitmapData(bounds.width, bounds.height,false);
                                bitmapData.copyPixels(stageBmd,bounds,new Point(0,0));


                                if(_hasHandWrite)
                                {
                                    var point:Point = new Point();
                                    var tempBmd:BitmapData = _canvas.drawToBitmapData();
                                    bitmapData.copyPixels(tempBmd, tempBmd.rect, point);
                                    bitmapData.copyPixels(logoBitmapData, logoBitmapData.rect, point);
                                    trace("MERGE ALPHA")
                                }
                                bitmapData.encode(bitmapData.rect,new JPEGEncoderOptions(100),ba);
                            }


                            if (ExternalInterface.available) {
                                if(this._working)
                                    this._working.visible = true;

                                Button(_continueButton).isEnabled = false;
                                Button(_takeAgainButton).isEnabled = false;


                                // crear carpeta de session si no existe
                                var path:String = ExternalInterface.call("saveTempImage", Base64.encode(ba),StringUtil.format("output0.jpg"), "session");
                                trace("IMAGE SAVED TO: ",path);
                                framesFilePath.length = 0;
                                framesFilePath.push(path);



                                // indica al AIR, que tiene la session lista para redimensionar imagenes y luego guardar el GIF a enviar a S3, vaciar carpeta
                                appModel.localConnection.send("app#com.dedosmedia.Keshot","endSession",{ session:sessionCode, projectPath:appModel.url, files: framesFilePath, input:appModel.inputFilesPath});



                                ExternalInterface.call("saveMimeImage", Base64.encode(ba));
                            }
                        }

                    break;
			}
		}


    private function loadFrame():void
    {
        trace("loadFrame ")

        framesFilePath.push(StringUtil.format("temp/output{0}.{1}",currentGIFFrame,appModel.config.outputModule));
        frameLoader.load(new URLRequest(StringUtil.format("temp/output{0}.{1}",currentGIFFrame,appModel.config.outputModule)));
        frameLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,  onFrameLoaded);
    }

    // Carga la imagen de logo.png por si se llega a requerir poner sobre la imagen final
    private function loadLogo():void
    {
        trace("load Logo ")

        logoLoader.load(new URLRequest(StringUtil.format("Graphics/logo.png")));
        logoLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,  onLogoLoaded);
    }


    function onLogoLoaded(event:*):void {
        trace("Logo Loaded");
        var loaderInfo:LoaderInfo = LoaderInfo(event.target);

        var bitmap:Bitmap = loaderInfo.content as Bitmap;
        var bitmapData = bitmap.bitmapData;
        logoBitmapData = new BitmapData(bitmapData.width, bitmapData.height)
        logoBitmapData = bitmapData.clone();
    }



    function onFrameLoaded(event:*):void
    {
        trace("Frame Loaded");
        var loaderInfo:LoaderInfo = LoaderInfo(event.target);

        var bitmap:Bitmap = loaderInfo.content as Bitmap;
        var bitmapData = bitmap.bitmapData;


        if(_hasHandWrite)
        {
            var point:Point = new Point();
            //Aqui si es WriteOn en GIF, debemos poner el WriteOn sobre la imagen
            var tempBmd:BitmapData = _canvas.drawToBitmapData();
            bitmapData.copyPixels(tempBmd, tempBmd.rect, point);
            bitmapData.copyPixels(logoBitmapData, logoBitmapData.rect, point);
            trace("MERGE ALPHA")
        }




        var imgByteArray:ByteArray = new ByteArray();
        bitmapData.encode(bitmapData.rect,new JPEGEncoderOptions(),imgByteArray);
        bitmapData.dispose();
        if(frameByteArray.length > currentGIFFrame)
        {
            frameByteArray[currentGIFFrame].clear();
            frameByteArray[currentGIFFrame] = imgByteArray
        }
        else
        {
            frameByteArray.push(imgByteArray);
        }


        if(++currentGIFFrame < appModel.config.photosPerSession)
        {
            loadFrame();
        }
        else
        {
            trace("All frames loaded into ByteArray")
            saveGIFFrames();
        }
    }

        private function saveGIFFrames():void
        {



            for(var i:uint = 0, j:uint = 1; i < frameByteArray.length; i++, j++)
            {
                var data:String = Base64.encode(frameByteArray[i]);

                trace("SAVE GIFG:: ", j.toString())

                //var path:String = ExternalInterface.call("saveTempImage", data, StringUtil.format("output{0}.jpg", i), "session");
                //trace("IMAGE SAVED TO: ", path);

                // indica al AIR, que tiene la session lista para redimensionar imagenes y luego guardar el GIF a enviar a S3, vaciar carpeta
                if (j == frameByteArray.length)
                {
                    trace("SEND ENDSESSION ",framesFilePath);
                    appModel.localConnection.send("app#com.dedosmedia.Keshot","endSession",{ session:sessionCode, projectPath:appModel.url, files: framesFilePath,  input:appModel.inputFilesPath});
                }


                ExternalInterface.call("saveMimeImage", data, j.toString());
            }
        }

		override public function onRemove():void
		{
			trace("onRemove::FilterScreenMediator");
            appModel.localConnection.removeEventListener(AppEventType.APP_ERROR_LOCAL_CONNECTION,onLocalConnectionError);
            this._touchManager.removeEventListener(TouchSheetManager.DROPPED, onDrop);
            this._touchManager.removeEventListener(TouchSheetManager.DRAGGED, onDrag);
            this._touchManager.removeEventListener(TouchSheetManager.DEPTH_CHANGED, onDepthChanged);
            this._touchManager.dispose();
            if(bitmapData)
            {
                bitmapData.dispose();
                bitmapData = null;
            }


            if(_canvas)
            {
                this._canvas.removeEventListener(TouchEvent.TOUCH, onTouchCanvas);
            }


            if(this._preview_bmd_arr.length > 0)
            {
                for(var i:uint = 0, l:uint = this._preview_bmd_arr.length; i < l ; i++ )
                {
                    this._preview_bmd_arr[i].dispose();
                    this._preview_bmd_arr[i] = null;
                }
            }
            if(this._preview_texture_arr.length > 0)
            {
                for(var i:uint = 0, l:uint = this._preview_texture_arr.length; i < l ; i++ )
                {
                    this._preview_texture_arr[i].dispose();
                    this._preview_texture_arr[i] = null;
                }
            }

            if(Starling.juggler.contains(MovieClip(this._gif)))
            {
                Starling.juggler.remove(MovieClip(this._gif));
            }



			super.onRemove();
		}



	}
}