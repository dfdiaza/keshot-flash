package com.dedosmedia.utils
{
	public class Validate
	{
		public static function isValidName(nm:String):Boolean
		{
			//var nameExpression:RegExp = /[a-z]{2}/i;
			var nameExpression:RegExp = /.+/i
			//  /^[a-z ,.'-]+$/i
			return nameExpression.test(nm);
		}
		
		public static function isValidEmail(email:String):Boolean {
			var emailExpression:RegExp = /^[a-z][\w.-]+@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i;
			return emailExpression.test(email);
		}
		
		public static function isValidZip(zip:String):Boolean
		{
			var zipExpress:RegExp = /\d{3}/;
			return zipExpress.test(zip);
		}
		
		
		public static function isValidDate(date:String):Boolean
		{
			var month:String        = "(0?[1-9]|1[012])";
			var day:String          = "(0?[1-9]|[12][0-9]|3[01])";
			var year:String         = "([1-9][0-9]{3})";
			var separator:String    = "([.\/ -]{1})";
			
			var usDate:RegExp = new RegExp("^" + month + separator + day + "\\2" + year + "$");
			return (usDate.test(date));
		}
		
		public static function isValidYear(year:String):Boolean
		{
			var yearExpress:RegExp = /\d{4}/;
			return yearExpress.test(year);
		}		
		
		public static function isValidMonthDay(date:String):Boolean
		{
			var dateExpress:RegExp = /\d{2}/;
			return dateExpress.test(date);
		}	
		
		public static function isValidPhone(ph:String):Boolean
		{
			var phExpress:RegExp = /\d{4}/;
			return phExpress.test(ph);
		}
	}
}