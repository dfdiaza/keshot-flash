package com.dedosmedia.events
{
	public class FileEventType
	{
		public static const FILE_SAVE_COMPLETED:String = "fileSaveCompleted";
		public static const FILE_SAVE_ERROR:String = "fileSaveError";
		public static const FILE_SAVE_START:String = "fileSaveStart";
		
		public static const FILE_UPLOAD_START:String = "fileUploadStart";
		
		public static const FILE_COPY_ERROR:String = "fileCopyError";
		public static const FILE_COPY_COMPLETED:String = "fileCopyCompleted";
		public static const FILE_COPY_START:String = "fileCopyStart";


        public static const FILE_READ_ERROR:String = "fileReadError";
        public static const FILE_READ_COMPLETED:String = "fileReadCompleted";
        public static const FILE_READ_START:String = "fileReadStart";


        public static const FILE_READ_JSON_START:String = "fileReadJSONStart";



		
	}
}