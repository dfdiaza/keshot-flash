package com.dedosmedia.events
{
	public class AppEventType
	{
        public static const APP_CONFIGVO_START:String = "appConfigVOStart";
        public static const APP_CONFIGVO_UPDATED:String = "appConfigVOUpdated";

        public static const APP_NEXT_SCREEN:String = "appNextScreen";
        public static const APP_BACK_SCREEN:String = "appBackScreen";

        public static const APP_ERROR_LOCAL_CONNECTION:String = "appErrorLocalConnection";
	}
}


