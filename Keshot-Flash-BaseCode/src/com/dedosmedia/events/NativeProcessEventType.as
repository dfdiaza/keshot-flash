package com.dedosmedia.events
{
	public class NativeProcessEventType
	{

        public static const NATIVEPROCESS_START:String = "nativeProcessStart";
		public static const NATIVEPROCESS_ERROR:String = "nativeProcessError";
		public static const NATIVEPROCESS_COMPLETED:String = "nativeProcessCompleted";

		
	}
}