package com.dedosmedia.controller
{

import com.dedosmedia.model.AppModel;
import feathers.controls.Screen;
import flash.external.ExternalInterface;
import org.robotlegs.starling.mvcs.Command;
import starling.events.Event;
	
	public class ShowBackScreenCommand extends Command
	{	
		
		[Inject]
		public var event:Event;

		[Inject]
		public var appModel:AppModel;

		private var screen:Screen;
		
		override public function execute():void
		{
			
			trace("execute::ShowBackScreenCommand",event, event.data.screen.screenID, appModel.config.sessionType);

			screen = event.data.screen;

			// En que pantala estamos
			switch(screen.screenID)
			{
				case MainApp.START:
				case MainApp.START_PAPARAZZI:
						cancelFlash();
					break;
				case MainApp.FILTER:
						trace("SOY FILTER")
						switch(appModel.config.sessionType)
						{
                            case "paparazzi":
                            case "paparazzi1":
							case "paparazzi3":
                            case "paparazzi5":
                              		cancelFlash();
								break;
							default:
                               goBack();
						}
					break;
			}

		}

		private function goBack():void
		{
            screen.dispatchEventWith(Event.CLOSE);
		}

		private function cancelFlash():void
		{
			trace("Cancel Flash")
            if (ExternalInterface.available) {
				trace("salgase mejor")
                ExternalInterface.call("cancelFlash");
            }
            else
            {
                trace("SIMULAR CERRAR FLASH");
            }
		}
	}

}