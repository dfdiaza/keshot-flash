package com.dedosmedia.controller
{
	
	import com.dedosmedia.services.IFileReadService;
	import org.robotlegs.starling.mvcs.Command;
	import starling.events.Event;



    /*
    *   Se encarga de leer un fichero en formato JSON, indicado en el evento  event.data.file
    *   onComplete: despacha el event FILE_READ_COMPLETE
    * */
	public class FileReadJSONStartCommand extends Command
	{
		
		[Inject]
		public var event:Event;
		
		[Inject]
		public var service:IFileReadService;

		override public function execute():void
		{
            trace("FileRead command")
			service.read(event.data.file);
		}
	}

}