package com.dedosmedia.controller
{

import com.dedosmedia.events.AppEventType;
import com.dedosmedia.model.AppModel;

import flash.external.ExternalInterface;

import org.robotlegs.starling.mvcs.Command;
	import starling.events.Event;
	
	public class UpdateConfigVOCommand extends Command
	{
		
		[Inject]
		public var event:Event;
		
		[Inject]
		public var appModel:AppModel;
		
		override public function execute():void
		{

			var params:Object = event.data.params;
            var config:Object = event.data.config;


            appModel.config.language        =   params.currentLocalization      ||  config.language;
            appModel.config.sessionType     =   params.flashSessionTypes        ||  config.sessionType;
            appModel.config.selectedBackground     =   params.selectedBackground        ||  config.selectedBackground;
            appModel.config.gifDuration =       params.flashGifDuration              || config.gifDuration;

            var paparazziPicturePath:String =   params.paparazziPicturePath;

            if(paparazziPicturePath && paparazziPicturePath != "")
            {
                var items:Array = paparazziPicturePath.split("|");
                for(var i:uint = 0, l:uint = items.length; i<l; i++)
                {
                    appModel.config.paparazziPicturePath.push(items[i]);
                }
            }

            appModel.config.params =   params || appModel.config.params;
            appModel.config.automaticFilter =   config.automaticFilter || appModel.config.automaticFilter;
            appModel.config.automaticBackground =   config.automaticBackground || appModel.config.automaticBackground;
            appModel.config.automaticOverlay =   config.automaticOverlay || appModel.config.automaticOverlay;
            appModel.config.previewTimeBeforeCaptureNextPicture = config.previewTimeBeforeCaptureNextPicture || appModel.config.previewTimeBeforeCaptureNextPicture;
            appModel.config.showPreviewAfterCapture     = config.showPreviewAfterCapture || appModel.config.showPreviewAfterCapture;
            appModel.config.chooseColorFromLastPreview  = config.chooseColorFromLastPreview || appModel.config.chooseColorFromLastPreview;
            appModel.config.highLightCurrentFilter      = config.highLightCurrentFilter || appModel.config.highLightCurrentFilter;
            appModel.config.hideFrameBeforeCapturePicture      = config.hideFrameBeforeCapturePicture || appModel.config.hideFrameBeforeCapturePicture;
            appModel.config.isPictureDraggable = config.isPictureDraggable || appModel.config.isPictureDraggable;
            appModel.config.trimAlphaEnabled = config.trimAlphaEnabled != undefined ?  config.trimAlphaEnabled : appModel.config.trimAlphaEnabled;
            appModel.config.isFaceme = config.isFaceme != undefined ?  config.isFaceme : appModel.config.isFaceme;


            appModel.config.antialiasingEnabled = config.antialiasingEnabled || appModel.config.antialiasingEnabled;
            appModel.config.sendPictureFromAE = config.sendPictureFromAE || appModel.config.sendPictureFromAE;
            appModel.config.showOverlaySelection = config.showOverlaySelection || appModel.config.showOverlaySelection;

            appModel.config.selectedObject = config.defaultObject;  // Si está definido pero se escoge un overlay, se actualiza su valor


            trace("ANTIALIASING:: ",appModel.config.antialiasingEnabled, config.antialiasingEnabled)
            trace("SESSION: ",ExternalInterface.call("getVariable", "currentSessionType"));
            switch (appModel.config.sessionType)
            {
                /*
                // No existe ningun sessiontype de faceme
                case "faceme":
                        appModel.config.photosPerSession = 2;  // Este número debería sobreescribirse con  el número confiurado para el overlay elegido.
                        appModel.config.isFaceme = true;
                    break;
                 */
                case "1picture":
                    appModel.config.photosPerSession = 1;
                    break;
                case "4pictures":
                    appModel.config.photosPerSession = 4;
                    break;
                case "6picturesAnimated":
                    appModel.config.photosPerSession = 6;
                    appModel.config.isGIF = true;
                    break;
                case "4picturesAnimated":
                case "XpicturesAnimated":
                    appModel.config.isGIF = true;
                    appModel.config.photosPerSession = ExternalInterface.call("getVariable", "gifPhotos");
                    appModel.config.photosTimePerSession = ExternalInterface.call("getVariable", "gifPhotosTime");


                    trace("NUM PHOTOS ", appModel.config.photosPerSession, "TIME ",appModel.config.photosTimePerSession );
                    break;
                case "paparazzi":
                case "paparazzi1":
                case "paparazzi3":
                case "paparazzi5":
                    appModel.config.isPaparazzi = true;
                    //appModel.config.isGIF = true;
                    appModel.config.photosPerSession = appModel.config.paparazziPicturePath.length;
                    appModel.config.isGIF = appModel.config.photosPerSession>1?true:false;

                    // pueden haber paparazzi que no sean gif, de ser asó hab´ia que ajustar esto.

                    break;

            }
			trace(appModel.config.sessionType)


            this.dispatchWith(AppEventType.APP_CONFIGVO_UPDATED);
		}
	}

}