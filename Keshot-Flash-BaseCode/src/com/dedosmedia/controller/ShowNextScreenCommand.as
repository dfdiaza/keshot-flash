package com.dedosmedia.controller
{
	
	//import feathers.examples.weather.services.IFavoriteLocationsService;

import com.dedosmedia.events.AppEventType;
import com.dedosmedia.model.AppModel;

import feathers.controls.Screen;

import org.robotlegs.starling.mvcs.Command;
	
	import starling.events.Event;
	
	public class ShowNextScreenCommand extends Command
	{	
		
		[Inject]
		public var event:Event;

		[Inject]
		public var appModel:AppModel;
		
		override public function execute():void
		{
			
			trace("execute::ShowNextScreenCommand",event);

			var screen:Screen = event.data.screen;

			// En que pantala estamos
			switch(screen.screenID)
			{
                case MainApp.OVERLAY:

                    if(appModel.config.isPaparazzi)
					{
                        screen.dispatchEventWith(MainApp.SHOW_START_PAPARAZZI);
					}
					else if(appModel.config.isFaceme)
                    {
						trace("SHOW START FACEME");
                        screen.dispatchEventWith(MainApp.SHOW_START_FACEME);
                    }
					else
					{
                        screen.dispatchEventWith(MainApp.SHOW_START);
					}

                    break;
				case MainApp.START:
                case MainApp.START_FACEME:
				case MainApp.START_PAPARAZZI:
						// Averiguar el estado de la pantalla y definir que ruta tomar
						screen.dispatchEventWith(MainApp.SHOW_FILTER);
					break;

			}



			
			//service.save(event.data.text, event.data.file, event.data.mode);
		}
	}

}