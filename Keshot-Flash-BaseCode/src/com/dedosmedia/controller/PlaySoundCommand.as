package com.dedosmedia.controller
{
	
	import com.dedosmedia.model.AssetsModel;
	import org.robotlegs.starling.mvcs.Command;
	import starling.events.Event;
	
	public class PlaySoundCommand extends Command
	{
		
		[Inject]
		public var event:Event;
		
		[Inject]
		public var assetsModel:AssetsModel;
		
		override public function execute():void
		{
			assetsModel.assets.playSound(event.data as String);
		}
	}

}