package
{
///
import com.dedosmedia.AppContext;

import feathers.controls.StackScreenNavigator;

import starling.textures.Texture;

import starling.events.Event;
import com.demonsters.debugger.MonsterDebugger;
public class MainApp extends StackScreenNavigator
{

    public static const OVERLAY:String = "overlayScreen";
    public static const SHOW_OVERLAY:String = "show"+OVERLAY;

    public static const START:String = "startScreen";
    public static const SHOW_START:String = "show"+START;

    public static const START_PAPARAZZI:String = "startPaparazziScreen";
    public static const SHOW_START_PAPARAZZI:String = "show"+START_PAPARAZZI;

    public static const START_FACEME:String = "startFacemeScreen";
    public static const SHOW_START_FACEME:String = "show"+START_FACEME;

    public static const FILTER:String = "filterScreen";
    public static const SHOW_FILTER:String = "show"+FILTER;


    private var _context:AppContext;



    private var _url:String
    public function get url():String
    {
        return this._url;
    }
    public function set url(_url:String):void
    {
        this._url = _url;
    }

    private var _initConfig:Object;
    public function set initConfig(_value:Object):void
    {
        this._initConfig = _value;
        trace("initCOnfig 2 ",this._initConfig)
    }
    public function get initConfig():Object
    {
        return this._initConfig;
    }

    private var _params:Object;
    public function set params(_params:Object):void
    {
        this._params = _params;
    }
    public function get params():Object
    {
        return this._params;
    }



    /*
     *  ENTRY POINT
     *
     */

    public function MainApp()
    {
        this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
    }

    protected function addedToStageHandler(event:Event):void
    {
        // Start the MonsterDebugger with a Starling display object
        MonsterDebugger.initialize(this);
        MonsterDebugger.trace(this, "Hello World!");
        this.removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
    }

    public function start():void
    {
        this._context = new AppContext(this, true);
    }
}
}