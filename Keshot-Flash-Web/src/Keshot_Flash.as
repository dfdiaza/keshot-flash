package
{

import com.dedosmedia.model.vo.ConfigVO;

import flash.display.LoaderInfo;
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.UncaughtErrorEvent;
import flash.events.ErrorEvent;
import flash.external.ExternalInterface;
import flash.geom.Rectangle;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.system.Capabilities;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.ui.Multitouch;
import flash.ui.MultitouchInputMode;
import flash.system.Capabilities;
import flash.utils.setTimeout;

import org.gestouch.core.Gestouch;
import org.gestouch.extensions.starling.Starling2TouchHitTester;
import org.gestouch.extensions.starling.StarlingDisplayListAdapter;
import org.gestouch.input.NativeInputAdapter;
import org.gestouch.extensions.native.NativeDisplayListAdapter;

import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;
import starling.filters.FragmentFilter;
import starling.textures.TextureSmoothing;
import starling.utils.RectangleUtil;
import starling.utils.ScaleMode;
import starling.utils.SystemUtil;


[SWF(width="1080", height="1920", frameRate="60",  backgroundColor="#000000")]
public class Keshot_Flash extends Sprite
{


    private var _starling:Starling;

    private var _initConfig:Object;		// archivo de configuacion inicial

    private var _params:Object;       // Holds the parameters from the .html
    private var _lang:String; "en" ;    // The language we are displaying


    private var _config:ConfigVO = new ConfigVO();

    private var retryButton:Sprite = new Sprite();

    private var version:String = "v0.0.13";


    public function Keshot_Flash()
    {
        trace("Keshot_Flash start.")
        this.mouseEnabled = this.mouseChildren = false;

        if (stage)
        {
            handlerAddedToStage();
        }
        else
        {
            this.addEventListener(Event.ADDED_TO_STAGE, handlerAddedToStage);
        }
    }

    private function cleanFlash():void
    {

        trace("cleanFlash ");
    }

    protected function handlerAddedToStage(event:Event = null):void
    {
        this.removeEventListener(Event.ADDED_TO_STAGE, handlerAddedToStage);

        loaderInfo.uncaughtErrorEvents.addEventListener( UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtErrorHandler );

        stage.scaleMode = StageScaleMode.NO_SCALE;
        stage.align = StageAlign.TOP_LEFT;
        stage.addEventListener(Event.RESIZE, onResize);

        if(Capabilities.playerType == "StandAlone")
        {
            var myFormat:TextFormat = new TextFormat();
            myFormat.size = 30;
            myFormat.color = 0xFFFFFF;

            var text:TextField = new TextField();
            text.defaultTextFormat = myFormat;
            text.text = version;
            this.addChild(text);

        }



        retryButton.graphics.beginFill( 0x000000 );
        retryButton.graphics.drawRect( 0, 0, 1080,1920);
        retryButton.graphics.endFill();
        stage.addChild(retryButton);
        retryButton.visible = false;


        // Inicializar ConfigVO con los parametros recibidos desde el HTML.
        // Realmente necesitamos leer esos parametros desde el HTML? o podemos cargarlos desde un fichero externo
        // En el caso de flashSessionTypes es necesario leerlo desde aquí, ya que es dinamico segun seleccione el usuario y determina el modo en que funciona la app.

        this._params = LoaderInfo(this.root.loaderInfo).parameters;


        var rootPath = this.loaderInfo.loaderURL;
        trace("RootPath ",rootPath, this.loaderInfo.url);


        for (var i in this._params)
        {
            trace(i," : ",this._params[i]);
        }

        // Configurar Localizacion
        if(this._params.currentLocalization != undefined)
        {
            this._config.language = this._params.currentLocalization;
        }

        // Configurar sessionType
        if(this._params.flashSessionTypes != undefined)
        {
            this._config.sessionType = this._params.flashSessionTypes;
        }

        this.loaderInfo.addEventListener(Event.COMPLETE, loaderInfo_completeHandler);



    }



    private function loaderInfo_completeHandler(event:Event):void
    {

        var request:URLRequest = new URLRequest("content/json/init.json");
        var configLoader:URLLoader = new URLLoader(request);
        configLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError)
        configLoader.addEventListener(Event.COMPLETE, function(event:Event):void
        {
            try{
                _initConfig = JSON.parse(configLoader.data);
            }
            catch(e:Error)
            {
                trace("ERRROR: PARSING INIT.JSON FILE ",e.getStackTrace())
                return;
            }

            trace("content/json/init.json  loaded correctly. ", _initConfig);

            trace("player Type ",Capabilities.playerType)
            trace("STAGE SIZE::: ",stage.stageWidth,stage.stageHeight);
            if(Capabilities.playerType == "ActiveX"  || Capabilities.playerType != "ActiveX" )
            {
               tryStartStarling();

            }

        });
    }

    private function retryStartStarling():void
    {
        tryStartStarling();
    }


    private  function  tryStartStarling():void
    {
        if(stage.stageWidth == 0 || stage.stageHeight == 0)
        {
            trace("XXXXXXXXX ----- ERRROR SIZE IS ZERO.... WHY? ");
            setTimeout(retryStartStarling,500);
            retryButton.visible = true;
        }
        else
        {

            startStarling();
        }
    }

    private function onIOError(event:IOErrorEvent):void {
        trace("ERROR: Fichero init.json no existe")
    }

    private function startStarling():void
    {
        Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
        Starling.multitouchEnabled = false;


        var stageWidth:Number =  stage.stageWidth; //stage.fullScreenWidth;
        var stageHeight:Number = stage.stageHeight; //stage.fullScreenWidth;


        if(stageWidth == 0 || stageHeight == 0)
        {

            trace("XXXXXXXXXXXXXXXXXXXXXX --------------- ERROR --------------- XXXXXXXXXXXXXXXXXXXXXX");
            stageWidth =  1080; //stage.stageWidth; //stage.fullScreenWidth;
            stageHeight = 1920;
        }

        retryButton.visible = false;

        var stageSize:Rectangle  = new Rectangle(0, 0, stageWidth, stageHeight);  // que sea al tamaño de nuestro diseño
        var screenSize:Rectangle = new Rectangle(0, 0, stageWidth, stageHeight); //  la resolución de nuestra pantalal
        var viewPort:Rectangle = RectangleUtil.fit(stageSize, screenSize, ScaleMode.SHOW_ALL);


        trace("stageSize ",stageSize)
        trace("screenSize ",screenSize)
        trace("fullScreenSize", stage.fullScreenWidth, stage.fullScreenHeight)
        trace("viewPort", viewPort)

        this._starling = new Starling(MainApp,stage, viewPort);
//        this._starling.antiAliasing = 16;

        this._starling.addEventListener("rootCreated", starling_rootCreatedHandler);
        this.stage.addEventListener(Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);
        this._starling.start();


        if(Capabilities.isDebugger)
        {
            this._starling.showStats =  true;
            if(this._starling.showStats)
                this._starling.showStatsAt("left","bottom",1);
        }


        this._starling.skipUnchangedFrames = true;
        this._starling.simulateMultitouch = false;
        this._starling.supportHighResolutions = true;


        Gestouch.inputAdapter = new NativeInputAdapter(stage);

        // Evita un quiebre al usar gestos al stage de Flash
        Gestouch.addDisplayListAdapter(flash.display.DisplayObject, new NativeDisplayListAdapter());


        Gestouch.inputAdapter = new NativeInputAdapter(this.stage);
        Gestouch.addDisplayListAdapter(DisplayObject, new StarlingDisplayListAdapter());
        Gestouch.addTouchHitTester(new Starling2TouchHitTester(), -1);



    }

    private function starling_rootCreatedHandler(event:Object, root:DisplayObjectContainer):void
    {
        trace("inviisble "+stage.numChildren)

        trace("isDesktop ", SystemUtil.isDesktop, SystemUtil.isAIR)

        trace(Starling.current.root, root)

        var main:MainApp = Starling.current.root as MainApp;


        /*
        main.filter = new FragmentFilter();
        main.filter.antiAliasing = 16;
        main.filter.textureSmoothing = TextureSmoothing.TRILINEAR;
        */

        main.params = this._params;
        trace("intiConfig:: ",this._initConfig);
        main.url = this.loaderInfo.loaderURL;
        main.initConfig = this._initConfig;
        main.start();
    }



    private function onResize(event:Event):void
    {
        trace("[RESIZE EVENT]", stage.stageWidth, stage.stageHeight)//,stage.stageWidth, stage.stageHeight,stage.fullScreenWidth, stage.fullScreenHeight)


        trace(Capabilities.isDebugger, Capabilities.playerType, this._starling)
        if(Capabilities.isDebugger && Capabilities.playerType != "ActiveX" && !this._starling )
        {
            if(stage.stageWidth == 1080 && stage.stageHeight == 1920)
            {
                stage.removeEventListener(Event.RESIZE, onResize);
                startStarling();
            }

        }


    }


    private function stage_deactivateHandler(event:Event):void
    {
      //  this._starling.stop(true);
      //  this.stage.addEventListener(Event.ACTIVATE, stage_activateHandler, false, 0, true);
    }

    private function stage_activateHandler(event:Event):void
    {
     //   this.stage.removeEventListener(Event.ACTIVATE, stage_activateHandler);
     //   this._starling.start();
    }




    private function uncaughtErrorHandler( event:UncaughtErrorEvent ):void
    {
        trace("[UNCAUGHT EXCEPTION]")
        var errorText:String;
        var stack:String;
        if( event.error is Error )
        {
            errorText = (event.error as Error).message;
            stack = (event.error as Error).getStackTrace();
            if(stack != null){
                errorText += stack;
            }
        } else if( event.error is ErrorEvent )
        {
            errorText = (event.error as ErrorEvent).text;
        } else
        {
            errorText = event.text;
        }
        event.preventDefault();
        trace(errorText);
    }



}
}